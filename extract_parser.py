#! /usr/bin/env python

from lab.parser import Parser
def invalid(content,prop):
    if 'conditional input' in content:
        prop['invalid_input'] = 1
    else:
        prop['invalid_input'] = 0

def coverage(content,prop):
    if 'encode done' in content:
        prop['coverage'] = 1
    else:
        prop['coverage'] = 0

parser = Parser()
parser.add_pattern('encode_time','Encode Time: (\d+.\d+)s',type=float,required=False)
parser.add_pattern('candidate_time','Candidate Time: (\d+.\d+)s',type=float,required=False)
parser.add_pattern('encode_peak_memory','Encode Peak Memory: (\d+) KB',required=False)
parser.add_pattern('removed_operators','# of removed operators : (\d+)',required=False)
parser.add_pattern('translator_essentials','Translator essentials: (\d+)',required=False)
parser.add_pattern('translator_inessentials','Translator inessentials: (\d+)',required=False)
parser.add_function(invalid)
parser.add_function(coverage)
parser.parse()

