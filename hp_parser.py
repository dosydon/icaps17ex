#! /usr/bin/env python

from lab.parser import Parser
import re

def coverage(content,prop):
    if 'Solution Found!' in content:
        prop['coverage'] = 1
    else:
        prop['coverage'] = 0

def validate(content,prop):
    if 'Successful plans' in content:
        prop['validate'] = 1
    elif 'Failed plans' in content and prop['coverage'] == 1:
        prop['validate'] = -1
    else:
        prop['validate'] = 0

def config(content,prop):
    domain,instance,config = prop['id']
    prop['config'] = config
    prop['domain'] = domain
    prop['problem'] = instance

def maxrss(content,prop):
    prop["maxrss"] = None
    m = re.search('MAXRSS:(\d+)',content)
    if m:
        prop["maxrss"] = int(m.group(1))

def by_depth(content,prop):
    m = re.findall('depth:\d+ \d+\[ms\]',content)
    prop['by_depth'] = m

def by_depth_solved(content,prop):
    m = re.findall('depth:\d+ \d+\[ms\]',content)
    if prop['coverage'] == 1:
        prop['by_depth_solved'] = m

def makespan(content,prop):
    depth = re.findall('now at depth: (\d+)',content)
    if prop['coverage'] == 1 and len(depth) > 0:
        prop['makespan'] = int(depth[-1])

# def Makespan(content,prop):
#     if 'makespan' in prop:
#         prop['Makespan'] = prop['makespan']
#     else:
#         prop['Makespan'] = 39


def explored(content,prop):
    nums = re.findall('Explored (\d+) nodes',content)
    if nums:
        prop['explored'] = sum([int(num) for num in nums])
    else:
        prop['explored'] = None

def wall_clock_time(content,prop):
    prop['wall_clock_time'] = prop['hp_wall_clock_time']

parser = Parser()
parser.add_pattern('decode','DECODE    :(\d+.\d+)',type=float)
parser.add_function(config)
parser.add_function(coverage)
parser.add_function(validate)
parser.add_function(explored)
parser.add_function(maxrss)
parser.add_function(makespan)
parser.add_function(by_depth)
parser.add_function(by_depth_solved)
# parser.add_function(Makespan)
parser.add_function(wall_clock_time)
parser.parse()
