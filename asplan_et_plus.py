#! /usr/bin/env python

import os.path
import platform
import sys

from lab.experiment import Experiment
from lab.steps import Step
from lab.fetcher import Fetcher

from lab.environments import LocalEnvironment, MaiaEnvironment

from downward.experiment import FastDownwardExperiment
from downward.reports.absolute import AbsoluteReport
from downward.reports.scatter import ScatterPlotReport
from downward.reports.taskwise import TaskwiseReport
from downward.suites import suite_all,build_suite


FD = os.path.abspath("fd-es/fast-downward.py")
HP = os.path.abspath("asplan/asplan.py")
VAL = os.path.abspath("VAL/validate")
EXPPATH = 'data/asplan_et_plus/'
TIME_LIMIT = 300
MEM_LIMIT = 2048
DIR = os.path.dirname(os.path.abspath(__file__))
SUITE = ['miconic','pegsol-opt11-strips','satellite','tpp','barman-opt14-strips','blocks','depot','driverlog','freecell','mystery','parcprinter-opt11-strips','parking-opt14-strips','rovers','storage','tidybot-opt14-strips','woodworking-opt11-strips','airport','grid','gripper','scanalyzer-opt11-strips','sokoban-opt08-strips','visitall-opt14-strips']
ATTRIBUTES = ['coverage','validate']
REPO = os.path.expanduser('~/FD')
ENV = LocalEnvironment(processes=24)
BENCHMARKS = os.path.abspath("benchmarks")
CACHE_DIR = os.path.expanduser('lab')


exp = Experiment(EXPPATH,environment=ENV)
exp.add_resource('PARSER', 'clingo_parser.py','clingo_parser.py')

for suite in SUITE:
    for prob in build_suite(BENCHMARKS,[suite]):
        domain = prob.domain_file()
        instance = prob.problem_file()

        run = exp.add_run()
        config = 'ASPlAN+ET+'
        run.set_property('domain', suite)
        run.set_property('config', config)
        run.set_property('problem', prob.problem_file())
        run.add_resource('PROBLEM', instance)
        run.add_resource('DOMAIN', domain)
        run.add_command('hp', [HP,'DOMAIN','PROBLEM','--smart','--among','--opt_level','satisficing','--para_level','sequential','--axiom','--candidate_gen','top','--time_limit','{}'.format(TIME_LIMIT),'--mem_limit','{}'.format(MEM_LIMIT),'translate','--group_choice essential --axiom'])
        run.add_command('validate', [VAL,'DOMAIN','PROBLEM','sas_plan'])
        run.add_command('parse', ['PARSER'])
        run.set_property('id', [suite,instance,config])


exp.add_report(
    AbsoluteReport(attributes=ATTRIBUTES), outfile='report.html')
exp()
