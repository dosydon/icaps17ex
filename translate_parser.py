#! /usr/bin/env python

from lab.parser import Parser
# def default(content,prop):
#     if not 'translator_compute_essential' in prop:
#         prop['translator_compute_essential'] = 0
#     if not 'translator_compute_exact' in prop:
#         prop['translator_compute_exact'] = prop['translator_time_choosing_groups']
parser = Parser()
parser.add_pattern('translator_essentials','Translator essentials: (\d+)')
parser.add_pattern('translator_inessentials','Translator inessentials: (\d+)')
parser.add_pattern('translator_facts','Translator facts: (\d+)')
parser.add_pattern('translator_compute_essential','Computing essential groups: \[(\d+.\d+)s CPU,',type=float,required=False)
parser.add_pattern('translator_compute_exact','Computing exactly groups: \[(\d+.\d+)s CPU,',type=float,required=False)
# parser.add_function(default)

parser.parse()

