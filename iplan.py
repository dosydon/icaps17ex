#! /usr/bin/env python

import os
from lab.fetcher import Fetcher

from downward.reports.absolute import AbsoluteReport
from lab.experiment import Experiment
from lab.steps import Step
from lab.reports import Report
from lab.reports.filter import FilterReport
from lab.environments import LocalEnvironment
from downward.suites import suite_all,build_suite
from downward.reports.scatter import ScatterPlotReport



EXPPATH = 'data/iplan/'
VAL = os.path.abspath('VAL/validate')
HP = os.path.abspath('iplan/iplan.py')
TIME_LIMIT = 300
MEM_LIMIT = 2048
#SUITE = ['satellite']
SUITE = ['miconic','pegsol-opt11-strips','satellite','tpp','barman-opt14-strips','blocks','depot','driverlog','freecell','mystery','parcprinter-opt11-strips','parking-opt14-strips','rovers','storage','tidybot-opt14-strips','woodworking-opt11-strips','airport','grid','gripper','scanalyzer-opt11-strips','sokoban-opt08-strips','visitall-opt14-strips']
#SUITE = ['gripper:prob01.pddl']

BENCHMARKS = os.path.expanduser('benchmarks')
ENV=LocalEnvironment(processes=24)
ATTRIBUTES = ['decode','validate','coverage','makespan','hp_wall_clock_time','maxrss']
DIR = os.path.dirname(os.path.abspath(__file__))

exp = Experiment(EXPPATH,environment=ENV)

for suite in SUITE:
    for prob in build_suite(BENCHMARKS,[suite]):
        domain = prob.domain_file()
        instance = prob.problem_file()

        run = exp.add_run()
        config = 'IPLAN'
        run.set_property('domain', suite)
        run.set_property('config', config)
        run.set_property('problem', prob.problem_file())
        run.require_resource('PARSER')
        run.add_resource('PROBLEM', instance)
        run.add_resource('DOMAIN', domain)
        run.add_command('hp', [HP,'--single_thread','--opt_level','satisficing','--among','--time_limit','{}'.format(TIME_LIMIT),'--mem_limit','{}'.format(MEM_LIMIT),'DOMAIN','PROBLEM'])
        run.add_command('validate', [VAL,'DOMAIN','PROBLEM','sas_plan'])
        run.add_command('rm lp', ['rm','tmp_ub.lp'])
        run.set_property('id', [suite,instance,config])

exp.add_step(Step('hp-fetcher', Fetcher(), exp.path, exp.eval_dir,
                  parsers=os.path.join(DIR, 'hp_parser.py')))

exp.add_report(
    AbsoluteReport(attributes=ATTRIBUTES), outfile='report.html')

exp()
