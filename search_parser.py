#! /usr/bin/env python

from lab.parser import Parser
import re

def coverage(content,prop):
    if 'Solution found!' in content:
        prop['coverage'] = 1
    else:
        prop['coverage'] = 0

def valid(content,prop):
    if "Successful plans" in content:
        prop['valid'] = 1
    else:
        prop['valid'] = 0

parser = Parser()
parser.add_pattern('expansions_until_last_jump','Expanded until last jump: (\d+) state',required=False)
parser.add_pattern('expansions','Expanded (\d+) state',required=False)
parser.add_pattern('search_time','Search time: (\d+.\d+)s',type=float,required=False)
parser.add_pattern('raw_memory','Peak memory: (\d+) KB',required=False)

parser.add_function(coverage)
parser.add_function(valid)
parser.parse()
