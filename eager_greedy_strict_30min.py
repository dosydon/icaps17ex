#! /usr/bin/env python

import os.path
import platform
import downward
import subprocess
from lab.fetcher import Fetcher
from lab.steps import Step
from lab.environments import LocalEnvironment, MaiaEnvironment
from lab.steps import Step
from lab.reports import Report
from lab.experiment import Experiment
from downward.reports.absolute import AbsoluteReport
from downward.reports.taskwise import TaskwiseReport
from downward.reports.scatter import ScatterPlotReport
from downward.suites import suite_all,build_suite

EXPPATH = 'data/blind_strict_30min/'
BENCHMARKS = os.path.abspath('benchmarks')
FD = os.path.abspath('fastdownward.py')
DIR = os.path.dirname(os.path.abspath(__file__))
ATTRIBUTES = ['generated','coverage','expansions','evaluated','raw_memory','search_wall_clock_time']
#SUITE = ['sokoban-opt08-strips:p01.pddl']
SUITE = ['airport','grid','gripper','sokoban-opt08-strips','visitall-opt14-strips','miconic','pegsol-opt11-strips','satellite','scanalyzer-opt11-strips','tpp']
TIME_LIMIT=1800
MEM_LIMIT= 2048
CACHE_DIR = os.path.expanduser('~/lab')
DOWNWARD_SCRIPTS_DIR = os.path.join(CACHE_DIR,'downward/scripts')

ENV = LocalEnvironment(processes=24)
exp = Experiment(EXPPATH,environment=ENV)
exp.add_resource(
    'SEARCH_PARSER',
    os.path.join(DIR,'search_parser.py'))

for suite in SUITE:
    for prob in build_suite(BENCHMARKS,[suite]):

        config = 'blind axiom'
        run = exp.add_run()
        run.set_property('id', [suite,prob.problem_file(),config])
        run.set_property('domain', suite)
        run.set_property('config', config)
        run.set_property('problem', prob.problem_file())

        run.add_resource('PROBLEM', prob.problem_file())
        run.add_resource('DOMAIN', prob.domain_file())
        run.add_command('search', [FD,'--candidate_gen','top','--time_limit', '{}'.format(TIME_LIMIT),'--mem_limit','{}'.format(MEM_LIMIT),'DOMAIN','PROBLEM','translate','--group_choice essential --axiom'])
        run.add_command('search_parse', ['SEARCH_PARSER'])
#
        config = 'blind'
        run = exp.add_run()
        run.set_property('id', [suite,prob.problem_file(),config])
        run.set_property('domain', suite)
        run.set_property('config', config)
        run.set_property('problem', prob.problem_file())

        run.add_resource('PROBLEM', prob.problem_file())
        run.add_resource('DOMAIN', prob.domain_file())
        run.add_command('search', [FD,'--time_limit', '{}'.format(TIME_LIMIT),'--mem_limit','{}'.format(MEM_LIMIT),'DOMAIN','PROBLEM'])
        run.add_command('search_parse', ['SEARCH_PARSER'])

exp.add_step(Step('fetcher', Fetcher(), exp.path, exp.eval_dir,
                  parsers=os.path.join(DIR,'search_parser.py')))

exp.add_report(
    AbsoluteReport(attributes=ATTRIBUTES), outfile='report.html')

def get_domain(run1, run2):
    return run1['domain']

exp.add_report(
    ScatterPlotReport(
        attributes=['expansions'],
        get_category=get_domain,
        filter_config=['blind', 'blind axiom']),
    name='scatter_expansions',
    outfile=os.path.join('plots', 'expansions.png'))

exp()
