#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import json
from pprint import pprint
from collections import defaultdict


def has_specified_keyvals(item,**kwargs):
    for k, v in kwargs.items():
        if not item[k] == v:
            return False
    return True

def get_with_keyvals(data, **kwargs):
    return [v for k, v in data.items() if has_specified_keyvals(v, **kwargs)]

def get_coverage(inst_lst):
    return sum([item["coverage"] for item in inst_lst])

def get_all_solved(data):
    dic = defaultdict(lambda: True)
    for v in data.values():
        problem = v["problem"]
        if v["coverage"] == 0:
            dic[problem] = False
    return dic

def get_average_n(inst_lst, is_all_solved):
    solved = [item for item in inst_lst if is_all_solved[item["problem"]]]
    if len(solved) == 0:
        return "None"
    else:
        return "{0:.2f}".format((1.0 * sum([item["depth"] for item in solved])) / len(solved))

def get_average_decode(inst_lst):
    decoded = [item for item in inst_lst if float(item["DECODE"]) > 0]
    if len(decoded) == 0:
        return "None"
    else:
        return "{0:.2f}".format((1.0 * sum([item["DECODE"] for item in decoded])) / len(decoded))


def to_tex_table(table, rows, columns):
    for row in rows:
        line = ' & '.join([table[(row, 'name')]] + [table[(row,column)] for column in columns])
        print(line + ' \\\\')


