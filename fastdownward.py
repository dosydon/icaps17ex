#! /usr/bin/env python3
import time
import subprocess
import os
import signal
import argparse
import resource
from executor import Executor

FILE_DIR = os.path.dirname(os.path.realpath(__file__))
FD = os.path.join(FILE_DIR, 'fd-es/fast-downward.py')
FDAXIOM = os.path.join(FILE_DIR, 'fd-axiom/src/fast-downward.py')
EXT_HOME = os.path.join(FILE_DIR, 'tau_axiom_extractor')
class Command:
    def __init__(self,cmd,tag,**kwargs):
        self.cmd = cmd
        self.tag = tag
        self.kwargs = kwargs
    def proc(self,timeout=None,preexec_fn=None):
        return subprocess.Popen(self.cmd.split(' '),preexec_fn=preexec_fn,**self.kwargs)
    def __repr__(self):
        return (self.cmd)

class Tag:
    HP=1
    ENCODE=2
    DECODE=3
    TRANSLATE=4
    PREPROCESS=5
    VAL=6
    MZN=7
    CLINGO=8
    GRINGO=9
    CLASP=10
    MINION=11

class TranslateCmd(Command):
    def __init__(self,args):
        if hasattr(args, 'translate_args'):
            cmd = "{} --translate {} {} --translate-options {}".format(FD,args.dom_file,args.ins_file,args.translate_args)
        else:
            cmd = "{} --translate {} {}".format(FDAXIOM,args.dom_file,args.ins_file)
        tag = Tag.TRANSLATE
        super().__init__(cmd,tag)

class EncodeCmd(Command):
    def __init__(self,args):
        cmd = "{}/encode.py output.sas --candidate_gen {}".format(EXT_HOME,args.candidate_gen)
        tag = Tag.ENCODE
        super().__init__(cmd,tag)

class SearchCmd(Command):
    def __init__(self,args):
        cmd = "{} output --search {}".format(FD,args.search)
        tag = Tag.HP
        super().__init__(cmd,tag)

class FDPreprocessCmd(Command):
    def __init__(self,args):
        cmd = "{} --preprocess output.sas".format(FD)
        tag = Tag.HP
        super().__init__(cmd,tag)

def set_limits(args):
    signal.signal(signal.SIGALRM, sigalrm_handler)
    resource.setrlimit(resource.RLIMIT_AS,(args.mem_limit * 1024 * 1024,args.mem_limit * 1024 * 1024))
    signal.alarm(args.time_limit)

def sigalrm_handler(signum, frame):
    print('Signal handler called with signal', signum)
    raise TimeoutException("timeout")

class TimeoutException(Exception):
    pass

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("dom_file")
    parser.add_argument("ins_file")
    parser.add_argument("--candidate_gen",default='')
    parser.add_argument("--search",default='astar(blind())')
    parser.add_argument("--essential",action='store_true')
    parser.add_argument("--time_limit",help="depth",type=int,default=1800)
    parser.add_argument("--mem_limit",help="depth",type=int,default=2048)
    subparsers = parser.add_subparsers(help='sub-command help')
    translate_parser = subparsers.add_parser('translate',help=' help')
    translate_parser.add_argument('translate_args',help='translate args',type=str,default=' ')

    args = parser.parse_args()

    set_limits(args)

    log = []
    flag = True
    ex = Executor(log,args.time_limit)

    tr = ex.run(TranslateCmd(args))
    
    if len(args.candidate_gen) > 0:
        er = ex.run(EncodeCmd(args))
    ex.run(FDPreprocessCmd(args))
    ex.run(SearchCmd(args))
