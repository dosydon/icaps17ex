#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import json
from pprint import pprint
from collections import defaultdict
from json_to_table import *

configs = ['default','var based','graph based','essential','var based essential']
domains = ['miconic','pegsol-opt11-strips','satellite','scanalyzer-opt11-strips','tpp','barman-opt14-strips','blocks','depot','driverlog','freecell','mystery','parcprinter-opt11-strips','parking-opt14-strips','rovers','storage','tidybot-opt14-strips','woodworking-opt11-strips','airport','grid','gripper','sokoban-opt08-strips','visitall-opt14-strips']
columns = [('default','coverage'),('default','time'),('graph based','coverage'),('graph based','time'),('graph based','removed_operators'),('var based','coverage'),('var based','time'),('var based','removed_operators'),('essential','coverage'),('essential','coverage'),('essential','inessential variables'),('var based essential','coverage'),('var based essential','time'),('var based essential','removed_operators')]
def get_removed_operators(inst_lst):
    lst = ([item["removed_operators"] for item in inst_lst if "removed_operators" in item])
    if len(lst) > 0:
        return sum(lst)
    else:
        return 0

def instance_removed_operators(inst_lst):
    lst = ([item["removed_operators"] for item in inst_lst if "removed_operators" in item and item["removed_operators"] > 0])
    return len(lst)

def get_wall_clock_time(inst_lst, is_all_solved):
    lst = ([item["encode_wall_clock_time"] for item in inst_lst if ("encode_wall_clock_time" in item and is_all_solved[item["problem"]])])
    if len(lst) > 0:
        return '{0:.2f}'.format(1.0 * sum(lst) / len(lst))
    else:
        return None

def get_inessetial(inst_lst):
    lst = ([item["translator_inessentials"] for item in inst_lst if "translator_inessentials" in item])
    if len(lst) > 0:
        return sum(lst)
    return None

def instance_inessetial(inst_lst):
    lst = ([item for item in inst_lst if "translator_inessentials" in item and item["translator_inessentials"] > 0])
    return len(lst)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("properties",help="json file to read")
    args = parser.parse_args()

    table = {}

    with open(args.properties) as f:    
        data = json.load(f)
        is_all_solved = get_all_solved(data)

        for config in configs:
            for domain in domains:
                inst_lst = (get_with_keyvals(data, config = config, domain = domain))
                table[(domain, (config, 'coverage'))] = str(get_coverage(inst_lst))
                table[(domain, (config, 'removed_operators'))] = "{}/{}".format(str(get_removed_operators(inst_lst)), instance_removed_operators(inst_lst))
                table[(domain, (config, 'inessential variables'))] = "{}/{}".format(str(get_inessetial(inst_lst)), instance_inessetial(inst_lst))
                table[(domain, (config, 'time'))] = str(get_wall_clock_time(inst_lst,is_all_solved))
                table[(domain,'name')] = '{}({})'.format(domain,len(inst_lst))

            inst_lst = (get_with_keyvals(data, config = config))
            table[('sum', (config, 'coverage'))] = str(get_coverage(inst_lst))
            table[('sum', (config, 'removed_operators'))] = str(get_removed_operators(inst_lst))
            table[('sum', (config, 'time'))] = str(get_wall_clock_time(inst_lst,is_all_solved))
            table[('sum', (config, 'inessential variables'))] = str(get_inessetial(inst_lst))
            table[('sum','name')] = '{}({})'.format('sum',len(inst_lst))

    to_tex_table(table, domains + ['sum'], columns)
