#! /usr/bin/env python

""" Example lab experiment that approximates the number pi.  This file contains the advanced version of the experiment where pi is calculated with increasing precision.  This experiment builds on the basic pi.py experiment.  """ 
import os
from lab.fetcher import Fetcher

from downward.reports.absolute import AbsoluteReport
from lab.experiment import Experiment
from lab.steps import Step
from lab.reports import Report
from lab.reports.filter import FilterReport
from lab.environments import LocalEnvironment
from downward.suites import suite_all,build_suite
from downward.reports.scatter import ScatterPlotReport


EXPPATH = 'data/extract_fetcher/'
TIME_LIMIT = 300
MEM_LIMIT = 2048

ATTRIBUTES = ['translator_essentials','translator_inessentials','coverage','invalid_input','encode_returncode','encode_peak_memory','encode_wall_clock_time',"removed_operators",'candidate_time']
BENCHMARKS = os.path.expanduser('~/benchmarks')
ENV=LocalEnvironment(processes=24)
DIR = os.path.dirname(os.path.abspath(__file__))

exp = Experiment(EXPPATH,environment=ENV)

exp.add_step(Step('extract-fetcher', Fetcher(), 'data/extract', exp.eval_dir, parsers=os.path.join(DIR, 'extract_parser.py')))
exp.add_step(Step('extract-fetcher', Fetcher(), 'data/extract2', exp.eval_dir, parsers=os.path.join(DIR, 'extract_parser.py')))

exp.add_report(AbsoluteReport(attributes=ATTRIBUTES), outfile='report.html')

exp()
