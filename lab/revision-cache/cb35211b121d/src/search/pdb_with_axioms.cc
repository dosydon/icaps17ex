
#include "pdb_with_axioms.h"
#include "pdbs/util.h"
#include "asp_check.h"

#include "priority_queue.h"
#include "plugin.h"
#include "timer.h"

#include <map>
#include <queue>
#include <algorithm>
#include <limits>

//#define TRACE_PRINT_LOTS

// compute the hash value (index) of an abstract state; the
// abstract state is an array of values of the pattern variables
// only.
size_t PDBWithAxioms::hash_abstract(const std::vector<int> & state) const
{
  assert(state.size() == pattern.size());
  size_t index = 0;
  for (size_t i = 0; i < pattern.size(); ++i) {
    index += hash_multipliers[i] * state[i];
  }
  return index;
}

void PDBWithAxioms::print_abstract_state(const std::vector<int> & state) const
{
  assert(state.size() == pattern.size());
  size_t index = hash_abstract(state);
  cout << index << ":";
  for (size_t i = 0; i < pattern.size(); ++i) {
    int var = pattern[i];
    int val = state[i];
    cout << " " << g_fact_names[var][val] << " [" << var << "=" << val << "]";
  }
  cout << endl;
}

// check if primary condition is satisfied in abstract state.
bool PDBWithAxioms::abs_applicable_primary
(const std::vector<GlobalCondition>& pre, const std::vector<int> & state) const
{
  assert(state.size() == pattern.size());
  for (unsigned int i = 0; i < pre.size(); i++) {
    int cvar = pre[i].var;
    int cval = pre[i].val;
    if (g_axiom_layers[cvar] < 0) {
      int pvar = find_in_pattern(cvar);
      if (pvar >= 0) {
	if (state[pvar] != cval) return false;
      }
    }
  }
  return true;
}

// copy secondary conditions from cond to sec_cond
void PDBWithAxioms::get_secondary_condition
(const std::vector<GlobalCondition>& cond,
 std::vector<GlobalCondition>& sec_cond) const
{
  for (unsigned int i = 0; i < cond.size(); i++)
    if (g_axiom_layers[cond[i].var] >= 0)
      sec_cond.push_back(cond[i]);
}

// return index of var in pattern, -1 if var not in pattern.
int PDBWithAxioms::find_in_pattern(int var) const
{
  for (unsigned int i = 0; i < pattern.size(); i++)
    if (pattern[i] == var) return i;
  return -1;
}

void PDBWithAxioms::create_pdb
(const std::vector<int> &operator_costs)
{
  Timer pdb_construction_time;
  // idea: perform an exhaustive forwards exploration from the initial
  // state, and record "cheapest link to/from" and goal status for each
  // reached state; then, we trace back along those links to get
  // cheapest cost to goal. the reason for doing this is that testing
  // applicability and goal status is easier that way (not sure how to
  // do regression in this setting).
  ASPChecker checker;

  // find the set of operators that are relevant to the PDB
  std::vector<int> relevant_ops;
  for (unsigned int i = 0; i < g_operators.size(); i++) {
    if (is_operator_relevant(g_operators[i])) {
      relevant_ops.push_back(i);
#ifdef TRACE_PRINT_LOTS
      cout << g_operators[i].get_name() << " is relevant" << endl;
#endif
    }
  }

  // links encodes reverse links: (j, c) is in links[i] if there is
  // a link from i to j with cost c, where i,j are PDB indices.
  std::vector< std::map<int, int> > links(num_states);

  int n_var = pattern.size();

  // need to convert the goal into a GlobalCondition vector; also
  // split it into primary/secondary parts:
  std::vector<GlobalCondition> primary_goal;
  std::vector<GlobalCondition> secondary_goal;
  for (unsigned int i = 0; i < g_goal.size(); i++) {
    int gvar = g_goal[i].first;
    int gval = g_goal[i].second;
    if (g_axiom_layers[gvar] < 0)
      primary_goal.push_back(GlobalCondition(gvar, gval));
    else
      secondary_goal.push_back(GlobalCondition(gvar, gval));
  }

  // phase 1: forward exploration
  std::vector<int> abs_init_state(n_var, 0);
  for (int i = 0; i < n_var; i++)
    abs_init_state[i] = g_initial_state()[pattern[i]];

  std::vector<bool> reached(num_states, false);
  std::vector<bool> is_goal(num_states, false);
  std::deque< std::vector<int> > queue;
  queue.push_back(abs_init_state);
  reached[hash_abstract(abs_init_state)];
  while (queue.size() > 0) {
    // dequeue a new state: this state is (a) reachable from the initial
    // state and (b) not previously explored.
    const std::vector<int>& state = queue.front();
    size_t index = hash_abstract(state);
#ifdef TRACE_PRINT_LOTS
    print_abstract_state(state);
#endif
    // check if the state is a goal:
    is_goal[index] = abs_applicable_primary(primary_goal, state);
    if (is_goal[index] && (secondary_goal.size() > 0)) {
      is_goal[index] = checker.check_projection(pattern, state,
						secondary_goal);
    }
#ifdef TRACE_PRINT_LOTS
    cout << "is_goal = " << is_goal[index] << endl;
#endif
    // check all actions applicable in the state and add/update links:
    for (unsigned int k = 0; k < relevant_ops.size(); k++) {
      const GlobalOperator& op = g_operators[relevant_ops[k]];
      int op_cost = (operator_costs.empty() ?
		     get_adjusted_cost(op) : operator_costs[relevant_ops[k]]);
      bool is_applicable =
	abs_applicable_primary(op.get_preconditions(), state);
      if (is_applicable) {
	std::vector<GlobalCondition> sec_pre;
	get_secondary_condition(op.get_preconditions(), sec_pre);
	if (sec_pre.size() > 0)
	  is_applicable = checker.check_projection(pattern, state, sec_pre);
	if (is_applicable) {
	  // construct new state: apply unconditional effects
	  std::vector<int> new_state = state;
	  const std::vector<GlobalEffect>& eff = op.get_effects();
	  for (unsigned int i = 0; i < eff.size(); i++) {
	    assert(eff[i].conditions.size() == 0);
	    int j = find_in_pattern(eff[i].var);
	    if (j >= 0)
	      new_state[j] = eff[i].val;
	  }
#ifdef TRACE_PRINT_LOTS
	  cout << op.get_name() << " leads to ";
	  print_abstract_state(new_state);
#endif
	  size_t new_index = hash_abstract(new_state);
	  if (new_index != index) {
	    std::map<int, int>::iterator p = links[new_index].find(index);
	    if (p == links[new_index].end()) {
	      links[new_index][index] = op_cost;
	    }
	    else if (op_cost < p->second) {
	      links[new_index][index] = op_cost;
	    }
	    if (!reached[new_index]) {
	      queue.push_back(new_state);
	      reached[new_index] = true;
	    }
	  }
	}
      }
    }
    queue.pop_front();
  }

#ifdef TRACE_PRINT_LOTS
  for (size_t index = 0; index < num_states; index++) {
    cout << index << "(" << is_goal[index] << "):";
    for (std::map<int, int>::iterator p = links[index].begin();
	 p != links[index].end(); p++) {
      size_t predecessor = p->first;
      int cost = p->second;
      cout << " " << predecessor << "(" << cost << ")";
    }
    cout << endl;
  }
#endif

  // phase 2: backward exploration
  distances.reserve(num_states);
  // first (implicit) entry: priority,
  // second entry: index for an abstract state
  AdaptiveQueue<size_t> pq;

  // initialize queue
  for (size_t state_index = 0; state_index < num_states; ++state_index) {
    if (is_goal[state_index]) {
      pq.push(0, state_index);
      distances.push_back(0);
    }
    else {
      distances.push_back(numeric_limits<int>::max());
    }
  }

  // Dijkstra loop
  while (!pq.empty()) {
    pair<int, size_t> node = pq.pop();
    int distance = node.first;
    size_t state_index = node.second;
    if (distance > distances[state_index]) {
      continue;
    }

    // regress abstract_state
    for (std::map<int, int>::iterator p = links[state_index].begin();
	 p != links[state_index].end(); p++) {
      size_t predecessor = p->first;
      int alternative_cost = distances[state_index] + p->second;
      if (alternative_cost < distances[predecessor]) {
	distances[predecessor] = alternative_cost;
	pq.push(alternative_cost, predecessor);
      }
    }
  }

#ifdef TRACE_PRINT_LOTS
  for (size_t index = 0; index < num_states; index++) {
    cout << index << ": " << distances[index] << endl;
  }
#endif
  cout << "PDB construction time: " << pdb_construction_time()
       << " seconds" << endl;
  cout << "ASP calls: " << checker.number_of_calls() << endl;
  cout << "ASP total time: " << checker.total_time() << endl;
  cout << "ASP solve time: " << checker.solve_time() << endl;
}

void PDBWithAxioms::set_pattern
(const std::vector<int> &pat, const std::vector<int> &operator_costs)
{
  assert(is_sorted_unique(pat));
  pattern = pat;
  hash_multipliers.reserve(pattern.size());
  num_states = 1;
  for (size_t i = 0; i < pattern.size(); ++i) {
    assert(g_axiom_layers[pattern[i]] < 0); // only primary vars in pattern!
    hash_multipliers.push_back(num_states);
    num_states *= g_variable_domain[pattern[i]];
  }
  create_pdb(operator_costs);
}

// size_t PDBWithAxioms::hash_index(const GlobalState &state) const
// {
//   size_t index = 0;
//   for (size_t i = 0; i < pattern.size(); ++i) {
//     index += hash_multipliers[i] * state[pattern[i]];
//   }
//   return index;
// }

PDBWithAxioms::PDBWithAxioms
(const Options &opts, bool dump, const std::vector<int> &operator_costs)
  : PDBHeuristic(opts)
{
  verify_no_conditional_effects();
  assert(operator_costs.empty() || operator_costs.size() == g_operators.size());

  // Timer timer;
  set_pattern(opts.get_list<int>("pattern"), operator_costs);
  // if (dump) {
  //   cout << "PDB construction time: " << timer << endl;
  // }
}

PDBWithAxioms::~PDBWithAxioms()
{
  // no explicit destruction necessary
}

static Heuristic *_parse(OptionParser &parser) {
  parser.document_synopsis("PDB heuristic with axioms", "TODO");
  parser.document_language_support("action costs", "supported");
  parser.document_language_support("conditional effects", "not supported");
  parser.document_language_support("axioms", "supported");
  parser.document_property("admissible", "yes");
  parser.document_property("consistent", "yes");
  parser.document_property("safe", "yes");
  parser.document_property("preferred operators", "no");

  Heuristic::add_options_to_parser(parser);
  Options opts;
  parse_pattern(parser, opts);

  if (parser.dry_run())
    return 0;

  return new PDBWithAxioms(opts, true, std::vector<int>());
}

static Plugin<Heuristic> _plugin("pdb_with_axioms", _parse);
