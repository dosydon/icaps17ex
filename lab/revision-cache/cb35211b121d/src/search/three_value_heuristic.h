#ifndef THREE_VALUE_HEURISTIC_H
#define THREE_VALUE_HEURISTIC_H

#include "heuristic.h"

class ThreeValueHeuristic : public Heuristic {
  
    //bool relaxed_reachable(const State &state, std::vector<int> set_of_actions);
  
    int h_used;
    std::vector<int> primary_variables;
    std::vector<int> secondary_variables;
    
    std::vector< std::vector<int> > compute_landmarks(const GlobalState &state, const std::vector< std::vector <int> > &collectionL);
    int compute_lmcut_equivalent(const GlobalState &state);
    int compute_hplus(const GlobalState &state, const std::vector<std::vector<int> > &collectionL);
    int compute_hmax(const GlobalState &state);
    
protected:
    virtual void initialize();
    virtual int compute_heuristic(const GlobalState &state);
    
public:
    ThreeValueHeuristic(const Options &options);
    ~ThreeValueHeuristic();
};

//enumeration of values
enum values_t { tvfalse=0, tvtrue, tvunknown };

class Relaxed_planning_graph_layer{
  Relaxed_planning_graph_layer *next_layer;
  Relaxed_planning_graph_layer *previous_layer;
  int n_actions_used; //number of actions used in computing this layer
  
  //int n_operators;
  //int *applied_operators; //a list of applied operators.
  //Not sure whether the operators that were used to get from the previous layer to this one, or all the 
  //operators that were used so far.
  
  //extern std::vector<std::string> g_variable_name; //some other way to do this?
  //int n_variables=g_variable_name.size();

public:
  Relaxed_planning_graph_layer(Relaxed_planning_graph_layer *previous);
  ~Relaxed_planning_graph_layer();

  int n_layer; //number of layer - h_max is the n_layer of the layer in which goal have been achieved  
  std::vector<int> values; //a list of values representing a relaxed state
  std::vector<std::vector <bool> > primary_values;
  std::vector<int> used_actions; //vector constaining information about which actions were used
  
  std::vector<int> primary;
  std::vector<int> secondary;
  
  //int *values; // a list of values
  //int *used_actions; //a list of booleans telling whether an individual action has been used or not 
  void update_values(std::vector<int> &new_values);
  void print_layer();
  void string_layer();
  void make_start_layer(const GlobalState &state);
  void add_new_layer(const std::vector<int> &set_of_actions);
  void asp_add_new_layer(const std::vector<int> &set_of_actions);
  void set_n_layer(int n);
  int get_n_layer();
  //void add_current_values(int *current);
  void use_single_action(int &action);
  bool applicable_actions_remaining(); //there are still unused actions
  bool goals_reached();
  bool asp_goals_reached();
};

/*
class Relaxed_planning_graph{
  
  std::vector<Relaxed_planning_graph_layer *> graph;
  Relaxed_planning_graph_layer *start_layer;
  Relaxed_planning_graph_layer *current_layer;
  
public:
  
  
  
}*/

//three value relaxed axiom evaluator - works similar as the axiom evaluator except takes the relaxed values into account.

/*
class TVRelaxedAxiomEvaluator {
    struct AxiomRule;
    struct AxiomLiteral {
        std::vector<AxiomRule *> condition_of;
    };
    struct AxiomRule {
        int condition_count;
        int unsatisfied_conditions;
        int effect_var;
        int effect_val;
        AxiomLiteral *effect_literal;
        AxiomRule(int cond_count, int eff_var, int eff_val, AxiomLiteral *eff_literal)
            : condition_count(cond_count), unsatisfied_conditions(cond_count),
              effect_var(eff_var), effect_val(eff_val), effect_literal(eff_literal) {
        }
    };
    struct NegationByFailureInfo {
        int var_no;
        AxiomLiteral *literal;
        NegationByFailureInfo(int var, AxiomLiteral *lit)
            : var_no(var), literal(lit) {}
    };

    std::vector<std::vector<AxiomLiteral> > axiom_literals;
    std::vector<AxiomRule> rules;
    std::vector<std::vector<NegationByFailureInfo> > nbf_info_by_layer;

    // The queue is an instance variable rather than a local variable
    // to reduce reallocation effort. See issue420.
    std::vector<AxiomLiteral *> queue;
public:
    AxiomEvaluator();
    void evaluate(PackedStateBin *buffer);
};
*/


#endif
