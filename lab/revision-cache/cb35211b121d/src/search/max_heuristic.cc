#include "max_heuristic.h"

#include "global_operator.h"
#include "global_state.h"
#include "option_parser.h"
#include "plugin.h"

#include <cassert>
#include <vector>
using namespace std;




/*
  TODO: At the time of this writing, this shares huge amounts of code
        with h^add, and the two should be refactored so that the
        common code is only included once, in so far as this is
        possible without sacrificing run-time. We may want to avoid
        virtual calls in the inner-most loops; maybe a templated
        strategy pattern is an option. Right now, the only differences
        to the h^add code are the use of max() instead of add() and
        the lack of preferred operator support (but we might actually
        reintroduce that if it doesn't hurt performance too much).
 */

// construction and destruction
HSPMaxHeuristic::HSPMaxHeuristic(const Options &opts)
    : RelaxationHeuristic(opts) {
}

HSPMaxHeuristic::~HSPMaxHeuristic() {
}

// initialization
void HSPMaxHeuristic::initialize() {
    cout << "Initializing HSP max heuristic..." << endl;
    RelaxationHeuristic::initialize();
}

void HSPMaxHeuristic::enqueue_if_necessary(Proposition *prop, int cost)
{
  assert(cost >= 0);
  if (prop->cost == -1 || prop->cost > cost) {
    prop->cost = cost;
    queue.push(cost, prop);
    // cout << "enqueueing " << prop->get_var_name()
    // 	 << "=" << prop->get_name()
    // 	 << " with cost " << cost << endl;
  }
  else {
    // cout << "NOT enqueueing " << prop->get_var_name()
    // 	 << "=" << prop->get_name() << endl;
  }
  assert((prop->cost != -1) && (prop->cost <= cost));
}

// heuristic computation
void HSPMaxHeuristic::setup_exploration_queue() {
    queue.clear();

    for (size_t var = 0; var < propositions.size(); ++var) {
        for (size_t value = 0; value < propositions[var].size(); ++value) {
            Proposition &prop = propositions[var][value];
            prop.cost = -1;
        }
    }

    // Deal with operators and axioms without preconditions.
    for (size_t i = 0; i < unary_operators.size(); ++i) {
        UnaryOperator &op = unary_operators[i];
        op.unsatisfied_preconditions = op.precondition.size();
        op.cost = op.base_cost; // will be increased by precondition costs

        if (op.unsatisfied_preconditions == 0)
            enqueue_if_necessary(op.effect, op.base_cost);
    }
}

void HSPMaxHeuristic::setup_exploration_queue_state(const GlobalState &state) {
  for (size_t var = 0; var < propositions.size(); ++var) {
    if (g_axiom_layers[var] >= 0) {
      int val = g_default_axiom_values[var];
      Proposition *init_prop = &propositions[var][val];
      enqueue_if_necessary(init_prop, 0);
    }
    else {
      Proposition *init_prop = &propositions[var][state[var]];
      enqueue_if_necessary(init_prop, 0);
    }
  }
}

void HSPMaxHeuristic::relaxed_exploration() {
    int unsolved_goals = goal_propositions.size();
    while (!queue.empty()) {
        pair<int, Proposition *> top_pair = queue.pop();
        int distance = top_pair.first;
        Proposition *prop = top_pair.second;
	// cout << "dequeued " << prop->get_var_name()
	//      << "=" << prop->get_name() << endl;
        int prop_cost = prop->cost;
        assert(prop_cost <= distance);
        if (prop_cost < distance)
            continue;
        if (prop->is_goal && --unsolved_goals == 0)
            return;
        const vector<UnaryOperator *> &triggered_operators =
            prop->precondition_of;
        for (size_t i = 0; i < triggered_operators.size(); ++i) {
            UnaryOperator *unary_op = triggered_operators[i];
            --unary_op->unsatisfied_preconditions;
            unary_op->cost = max(unary_op->cost,
                                 unary_op->base_cost + prop_cost);
            assert(unary_op->unsatisfied_preconditions >= 0);
            if (unary_op->unsatisfied_preconditions == 0) {
                enqueue_if_necessary(unary_op->effect, unary_op->cost);
	    }
	    // else {
	    //   if (unary_op->operator_no == -1) {
	    // 	cout << "not triggered axiom:";
	    // 	for (unsigned int k = 0; k < unary_op->precondition.size(); k++)
	    // 	  cout << " " << unary_op->precondition[k]->get_var_name()
	    // 	       << "=" << unary_op->precondition[k]->get_name();
	    // 	cout << " -> "<< unary_op->effect->get_var_name()
	    // 	     << "=" << unary_op->effect->get_name()
	    // 	     << endl;
	    //   }
	    //   else {
	    // 	cout << "not triggered: "
	    // 	     << g_operators[unary_op->operator_no].get_name()
	    // 	     << endl;
	    //   }
	    // }
        }
    }
}

int HSPMaxHeuristic::compute_heuristic(const GlobalState &state) {
    // cout << "computing hmax..." << endl;
    setup_exploration_queue();
    setup_exploration_queue_state(state);
    relaxed_exploration();

    int total_cost = 0;
    for (size_t i = 0; i < goal_propositions.size(); ++i) {
        int prop_cost = goal_propositions[i]->cost;
	// cout << "done: " << goal_propositions[i]->get_var_name()
	//      << "=" << goal_propositions[i]->get_name()
	//      << " has cost " << prop_cost << endl;
        if (prop_cost == -1)
            return DEAD_END;
        total_cost = max(total_cost, prop_cost);
    }

    // exit(0);
    return total_cost;
}

bool HSPMaxHeuristic::dead_ends_are_reliable() const
{
  return true;
}

static Heuristic *_parse(OptionParser &parser) {
    parser.document_synopsis("Max heuristic", "");
    parser.document_language_support("action costs", "supported");
    parser.document_language_support("conditional effects", "supported");
    parser.document_language_support(
        "axioms",
        "supported (in the sense that the planner won't complain -- "
        "handling of axioms might be very stupid "
        "and even render the heuristic unsafe)");
    parser.document_property("admissible", "yes for tasks without axioms");
    parser.document_property("consistent", "yes for tasks without axioms");
    parser.document_property("safe", "yes for tasks without axioms");
    parser.document_property("preferred operators", "no");

    Heuristic::add_options_to_parser(parser);
    Options opts = parser.parse();

    if (parser.dry_run())
        return 0;
    else
        return new HSPMaxHeuristic(opts);
}


static Plugin<Heuristic> _plugin("hmax", _parse);
