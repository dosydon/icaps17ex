
#include "three_value_heuristic.h"

#include "globals.h"
#include "option_parser.h"
#include "plugin.h"
#include "global_state.h"
#include "global_operator.h"

#include "asp_check.h"
//#include "asp_check.cc"

#include "axioms.h"

//#define USE_GUROBI
#define USE_TVR

#ifdef USE_GUROBI
#include "gurobi_c++.h"
#endif

#include <clasp/program_builder.h>   // for defining logic programs
#include <clasp/unfounded_check.h>   // unfounded set checkers
#include <clasp/model_enumerators.h> // for enumerating answer sets
#include <clasp/solve_algorithms.h>  // for enumerating answer sets



//#define TRACE_PRINT_API_CALLS
//#define PRINT_RELAXED_PLANNING_GRAPH
//#define TRACE_PRINT_MODEL

//Model printer

/*

struct ASPMPrinter : public Clasp::Enumerator::Report {
  const std::vector<int>& first_fact;
  bool model_found;
  ASPMPrinter(const std::vector<int>& ff)
    : first_fact(ff), model_found(false) { };
  void reportModel(const Clasp::Solver& s, const Clasp::Enumerator&);
  void reportSolution(const Clasp::Solver& s, const Clasp::Enumerator&, bool);
  const char* value_string(Clasp::ValueRep val) {
    if (val == Clasp::value_true)
      return "T";
    else if (val == Clasp::value_false)
      return "F";
    else
      return "U";
  };
};

void ASPMPrinter::reportModel
(const Clasp::Solver& s, const Clasp::Enumerator& e) {
  std::cout << "Model " << e.enumerated << ":" << std::endl;
  int n_vars = (int)g_variable_domain.size();
  cout << "n_vars: " << n_vars << endl;
  int n_facts = first_fact[n_vars];
  cout << "n_facts: " << n_facts << endl;
  int c_false = n_facts + 1;
  cout << "c_false: " << c_false << endl;
  for (unsigned int i = 0; i < g_variable_domain.size(); i++) {
    cout << "first_fact[i] " << first_fact[i] << endl;
    if (g_axiom_layers[i] < 0) {
      for (unsigned int j = 0; j < g_variable_domain[i]; j++)
	std::cout << g_variable_name[i] << "=" << g_fact_names[i][j]
		  << "(" << first_fact[i] + j << "): "
		  << value_string(s.value(first_fact[i] + j))
		  << std::endl;
    }
    else {
      std::cout << "(derived) " << g_variable_name[i]
		<< "(" << first_fact[i] << "): "
		<< value_string(s.value(first_fact[i]))
		<< std::endl;
    }
  }
  std::cout << "c_false (" << c_false << "): "
	    << value_string(s.value(c_false)) << std::endl;
  model_found = true;
}

void ASPMPrinter::reportSolution
(const Clasp::Solver&, const Clasp::Enumerator&, bool complete)
{
  // ignored
}

//the above stuff is for ASP, delete later.

*/


ThreeValueHeuristic::ThreeValueHeuristic(const Options &opts)
    : Heuristic(opts) {
      h_used=opts.get<int>("heuristic_used");
      cout << "Heuristic used: " << h_used << endl;
      
      //figuring out which of the variables are primary and which are secondary
      std::vector<int> p_vars(g_variable_domain.size(),0);
      std::vector<int> s_vars(g_variable_domain.size(),0);
      
      for (unsigned int i=0; i < g_operators.size(); i++) {
	assert(!g_operators[i].is_axiom());
	const std::vector<GlobalEffect> &action_effects=g_operators[i].get_effects();
	for(unsigned int j=0;j<action_effects.size();j++){
	  p_vars[action_effects[j].var]=1;
	}
      }
      
      for(unsigned int i=0; i < g_axioms.size(); i++) {
	const std::vector<GlobalEffect> &axiom_effects=g_axioms[i].get_effects();
	for(unsigned int j=0;j<axiom_effects.size();j++){
	  s_vars[axiom_effects[j].var]=1;
	}
      }
      primary_variables=p_vars;
      secondary_variables=s_vars;
      
   //
   //for(unsigned int i=0;i<g_operators.size();i++){
     //cout << g_operators[i].get_name(); cout << " " << i; 
     
   //  if(g_operators[i].get_name()=="stepup-tmin2-thro-vego-increase-from-tmin1-l-tmin2-l-t1-l-t2"){
   //    cout << "Found it!";
   //  }
   //  cout << endl;
   //}
   // 
   for(unsigned int i=0;i<g_variable_name.size();i++){
     cout << i << " " << g_variable_name[i] << " g_axiom_layers " << g_axiom_layers[i] << endl;
     /*
     << " Primary? " << primary_variables[i] << " Secondary? " << secondary_variables[i] << " s_vars? " 
     << s_vars[i] << " g_axiom_layers " << g_axiom_layers[i] << " g_default_axiom_values " << g_default_axiom_values[i] << endl;
     */
     if(primary_variables[i]==1){
       assert(g_axiom_layers[i]==-1);
     }
  }
}

ThreeValueHeuristic::~ThreeValueHeuristic() {
}

void ThreeValueHeuristic::initialize() {
    cout << "Initializing three value relaxed reachability heuristic..." << endl;
}

void Relaxed_planning_graph_layer::update_values(std::vector<int> &new_values){
  values=new_values;
}

bool is_landmark_empty(std::vector<int> l){
  for(unsigned int i=0;i<l.size();i++){
    if(l[i]!=0){
      //cout << "Returning false" << endl;
      return false;
    }
  }
  //cout << "Returning true" << endl;
  return true;
}

//returns whether the model is satisfiable
/*
int clasp_evaluate_axioms( 	//const std::vector<int> &relaxed_state_values,
				const std::vector<std::vector<bool> > &primary_vars_vector,
				const std::vector<GlobalCondition> &reqs
				){
  
  Clasp::ProgramBuilder api;
  Clasp::SharedContext ctx;
  api.startProgram(ctx);
   
#ifdef TRACE_PRINT_API_CALLS
  std::cout << "Clasp::ProgramBuilder api;" << std::endl;
  std::cout << "Clasp::SharedContext ctx;" << std::endl;
  std::cout << "api.startProgram(ctx);" << std::endl;
#endif
  
  int n_vars = (int)g_variable_domain.size();
  
  std::vector<int> first_fact(g_variable_domain.size() + 1, 1);
  
  for (unsigned int i = 0; i < g_axiom_layers.size(); i++) {
    if (g_axiom_layers[i] >= 0) {
      // this is a secondary (derived) variable: check that it
      // satisfies assumptions.
      assert(g_variable_domain[i] == 2);
      //assert(g_default_axiom_values[i] == 0);
      first_fact[i + 1] = first_fact[i] + 1;
    }
    else {
      // this is a primary variable
      first_fact[i + 1] = first_fact[i] + g_variable_domain[i];
    }
    //std::cout << "first_fact[" << i << "] = " << first_fact[i] << std::endl;
  }
  // if we implement some kind of incremental construction of the
  // ASP, more initialisations may be needed.

  
  int n_facts = first_fact[n_vars];
  int c_false = n_facts + 1;

  api.setCompute(c_false, false);
#ifdef TRACE_PRINT_API_CALLS
  std::cout << "api.setCompute(" << c_false << ", false);" << std::endl;
#endif
  
  //For each primary variable, make sure it has one of the possible values. NOTE: Slightly different than in pdbh.
  std::vector<bool> primary_var_values;
  for (int var = 0; var < n_vars; var++){ //for each primary variable
    if (g_axiom_layers[var] < 0) {	//check whether primary

     primary_var_values=primary_vars_vector[var];
      
     #ifdef TRACE_PRINT_API_CALLS
     std::cout << "// primary variable " << g_variable_name[var] << " var: " << var << std::endl;
     #endif
     
     api.startRule(Clasp::CHOICERULE); //variable must have one of the values in the relaxed state
     for (int val = 0; val < g_variable_domain[var]; val++){
      // if(primary_var_values[val]){
	  api.addHead(first_fact[var] + val);
      // }
     }
     api.endRule();
     
     #ifdef TRACE_PRINT_API_CALLS
     std::cout << "api.startRule(Clasp::CHOICERULE);" << std::endl;
     for (int val = 0; val < g_variable_domain[var]; val++){
	std::cout << "api.addHead(" << first_fact[var] + val << ");"
		  << std::endl;
     }
     std::cout << "api.endRule();" << std::endl;
      
     #endif
     
      // create an integrity constraint for each mutex pair of values:	//WARNING: Not sure whether to change this?
      for (int i = 0; i < g_variable_domain[var]; i++)
	for (int j = i + 1; j < g_variable_domain[var]; j++) {
	  api.startRule(Clasp::BASICRULE);
	  api.addHead(c_false);
	  api.addToBody(first_fact[var] + i, true);
	  api.addToBody(first_fact[var] + j, true);
	  api.endRule();
#ifdef TRACE_PRINT_API_CALLS
	  std::cout << "api.startRule(Clasp::BASICRULE);" << std::endl;
	  std::cout << "mutex constraint for " << var << std::endl;
	  std::cout << "api.addHead(" << c_false << ");" << std::endl;
	  std::cout << "api.addToBody(" << first_fact[var] + i << ", true);"
		    << std::endl;
	  std::cout << "api.addToBody(" << first_fact[var] + j << ", true);"
		    << std::endl;
	  std::cout << "api.endRule();" << std::endl;
#endif
     
    }
  
  
        // create an integrity constraint for at-least-1 value:
      // (FALSE :- not p, not q) <=> (p | q).
      api.startRule(Clasp::BASICRULE);
      api.addHead(c_false);
      for (int val = 0; val < g_variable_domain[var]; val++)
	api.addToBody(first_fact[var] + val, false);
      api.endRule();
#ifdef TRACE_PRINT_API_CALLS
      std::cout << "api.startRule(Clasp::BASICRULE);" << std::endl;
      std::cout << "at-least-one constraint for " << var << std::endl;
      std::cout << "api.addHead(" << c_false << ");" << std::endl;
      for (int val = 0; val < g_variable_domain[var]; val++)
	std::cout << "api.addToBody(" << first_fact[var] + val << ", false);"
		  << std::endl;
      std::cout << "api.endRule();" << std::endl;
#endif
    }
  }
  //Translate each axiom into a rule. //NOTE: Same as for pdbh?
  for (unsigned int k = 0; k < g_axioms.size(); k++) {
    const GlobalOperator& ax = g_axioms[k];
#ifdef TRACE_PRINT_API_CALLS
    std::cout << "// axiom #" << k << std::endl;
    std::cout << "// ";
    g_axioms[k].dump();
#endif
    // should be an axiom and have a single effect that sets a derived
    // variable to true; the precondition of the effect is only that
    // the variable is false, and the actual rule body is in the effect
    // condition.
    assert(ax.is_axiom());
    assert(ax.get_effects().size() == 1);
    const GlobalEffect& rule = ax.get_effects()[0];
    int rvar = rule.var;
    assert(g_axiom_layers[rvar] >= 0);
    assert(rule.val != g_default_axiom_values[rvar]);
    assert(ax.get_preconditions().size() == 1);
    assert(ax.get_preconditions()[0].var == rvar);
    assert(ax.get_preconditions()[0].val == g_default_axiom_values[rvar]);
    // make a rule:
    api.startRule(Clasp::BASICRULE);
    api.addHead(first_fact[rvar]);
#ifdef TRACE_PRINT_API_CALLS
    std::cout << "api.startRule(Clasp::BASICRULE);" << std::endl;
    std::cout << "api.addHead(" << first_fact[ax.get_effects()[0].var] << ");"
	      << std::endl;
#endif
    const std::vector<GlobalCondition>& body = rule.conditions;
    for (unsigned int j = 0; j < body.size(); j++) {
      int cvar = body[j].var;
      int cval = body[j].val;
      if (g_axiom_layers[cvar] < 0) {
	assert(cval < g_variable_domain[cvar]);
	api.addToBody(first_fact[cvar] + cval, true);
#ifdef TRACE_PRINT_API_CALLS
	std::cout << "api.addToBody(" << first_fact[cvar] + cval << ", true);"
		  << std::endl;
#endif
      }
      else if (cval != g_default_axiom_values[cvar]) {
	api.addToBody(first_fact[cvar], true);
#ifdef TRACE_PRINT_API_CALLS
	std::cout << "api.addToBody(" << first_fact[cvar] << ", true);"
		  << std::endl;
#endif
      }
      else {
	assert(cval == g_default_axiom_values[cvar]);
	api.addToBody(first_fact[cvar], false);
#ifdef TRACE_PRINT_API_CALLS
	std::cout << "api.addToBody(" << first_fact[cvar] << ", false);"
		  << std::endl;
#endif
      }
    }
    api.endRule();
#ifdef TRACE_PRINT_API_CALLS
    std::cout << "api.endRule();" << std::endl;
#endif
  }
  
  // add an integrity constraint to force the value of each variable
  // in the pattern: //NOTE: Not needed here, as representation of relaxed state is covered when dealing with primary variables.
  
  api.startRule(Clasp::BASICRULE);
  api.addHead(c_false);
  api.addToBody(first_fact[0] + 1, false);
  api.endRule();
  
  //for (unsigned int i = 0; i < g_variable_domain.size(); i++) {
    //assert(pattern[i] < n_vars);
    //int pvar = i;
    //assert(g_axiom_layers[pvar] < 0);
    //int pval = state[pvar];
    //int pval = state[i];
    //assert(pval < g_variable_domain[pvar]);
//#ifdef TRACE_PRINT_API_CALLS
  //  std::cout << "// " << g_variable_name[pvar] << " = " << pval << std::endl;
//#endif
  //  api.startRule(Clasp::BASICRULE);
    //api.addHead(c_false);
    //api.addToBody(first_fact[pvar] + pval, false);
    //api.endRule();
//#ifdef TRACE_PRINT_API_CALLS
  //  std::cout << "api.startRule(Clasp::BASICRULE);" << std::endl;
  //  std::cout << "api.addHead(" << c_false << ");" << std::endl;
  //  std::cout << "api.addToBody(" << first_fact[pvar] + pval << ", false);"
	      << std::endl;
  //  std::cout << "api.endRule();" << std::endl;
//#endif
//  }

  // for each required condition, add it's negation as an integrity constraint:
  //NOTE: Same as for pdbh.
  cout << "reqs.size() " << reqs.size() << endl;
  for (unsigned int i = 0; i < reqs.size(); i++) {
    int cvar = reqs[i].var;
    int cval = reqs[i].val;
    assert(g_axiom_layers[cvar] >= 0);
    assert(cval < g_variable_domain[cvar]);
#ifdef TRACE_PRINT_API_CALLS
    std::cout << "// require " << g_variable_name[cvar] << std::endl;
#endif
    api.startRule(Clasp::BASICRULE);
    api.addHead(c_false);
#ifdef TRACE_PRINT_API_CALLS
    std::cout << "api.startRule(Clasp::BASICRULE);" << std::endl;
    std::cout << "api.addHead(" << c_false << ");" << std::endl;
  //  std::cout << "api.endRule();" << std::endl;
#endif
    if (cval != g_default_axiom_values[cvar]) {
      api.addToBody(first_fact[cvar], false);
#ifdef TRACE_PRINT_API_CALLS
      std::cout << "api.addToBody(" << first_fact[cvar] << ", false);"
		<< std::endl;
#endif
    }
    else {
      assert(cval == g_default_axiom_values[cvar]);
      api.addToBody(first_fact[cvar], true);
#ifdef TRACE_PRINT_API_CALLS
      std::cout << "api.addToBody(" << first_fact[cvar] << ", true);"
		<< std::endl;
#endif
    }
    api.endRule();
#ifdef TRACE_PRINT_API_CALLS
    std::cout << "api.endRule();" << std::endl;
#endif
  }

  // ASP complete
  api.writeProgram(std::cout);
  bool ok = api.endProgram();
  api.writeProgram(std::cout);
#ifdef TRACE_PRINT_API_CALLS
  std::cout << "api.endProgram();" << std::endl;
#endif
  // endProgram returns false if the program is found to be inconsistent
  // in preprocessing; it seems that calling solve in this case causes
  // a segfault.
  if (!ok) {
    cout << "Returning false. (couldn't api.endProgram)" << endl;
    //abort();
    return false;
  }

  // stuff that has to be there...
  if (api.dependencyGraph() && api.dependencyGraph()->nodes() > 0) {
    Clasp::DefaultUnfoundedCheck* ufs = new Clasp::DefaultUnfoundedCheck();
    ufs->attachTo(*ctx.master(), api.dependencyGraph());
#ifdef TRACE_PRINT_API_CALLS
    std::cout << "Clasp::DefaultUnfoundedCheck* ufs = new Clasp::DefaultUnfoundedCheck();" << std::endl;
    std::cout << "ufs->attachTo(*ctx.master(), api.dependencyGraph());"
	      << std::endl;
#endif
  }

#ifdef TRACE_PRINT_MODEL
  ASPMPrinter printer(first_fact);
  ctx.addEnumerator(new Clasp::BacktrackEnumerator(0, &printer));
  ctx.enumerator()->enumerate(0);
#endif

  ctx.endInit();
  Clasp::SolveParams sparams;
  ok = Clasp::solve(ctx, sparams);
  cout << "ok? " << ok << endl;
#ifdef TRACE_PRINT_MODEL
  ok = printer.model_found; // return value doesn't work with enumerator
#endif
#ifdef TRACE_PRINT_API_CALLS
  std::cout << "ctx.endInit();" << std::endl;
  std::cout << "Clasp::SolveParams sparams;" << std::endl;
  std::cout << "bool ok = Clasp::solve(ctx, sparams);" << std::endl;
#endif

  // all deallocs are (I presume) automatic, so we can just return...
  cout << "ok? " << ok << endl;
  return ok;
}

*/

bool action_tvr_applicable(GlobalOperator &action,
			   std::vector<int> &relaxed_state_values,
			   std::vector<std::vector<bool> > &relaxed_state_primary_variables){
  const std::vector<GlobalCondition>& conditions=action.get_preconditions();
  int condition_variable;
  int condition_value;
  std::vector<bool> variable_vector;
  bool applicable, relaxed_applicable;	
  for(unsigned int i=0;i<conditions.size();i++){
    condition_variable=conditions[i].var;
    condition_value=conditions[i].val;
    variable_vector=relaxed_state_primary_variables[condition_variable];
    if(g_axiom_layers[condition_variable]==-1){		//if it's a primary variable, check whether condition_value is in the vactor
 //     if(not(std::find(variable_vector.begin(), variable_vector.end(), condition_value)!=variable_vector.end())){
      if(not(variable_vector[condition_value])){
	return false;	//if it's not, the action is not applicable
      }
    }else{						//if it's a secondary variable, check whether it's corresponding value or unknown
      applicable = relaxed_state_values[condition_variable]==condition_value;
      relaxed_applicable = relaxed_state_values[condition_variable]==g_variable_domain[condition_variable];
      if(not(applicable || relaxed_applicable)){
    //  if(not((relaxed_state_values[condition_variable]==condition_value) || (relaxed_state_values[var]==g_variable_domain[condition_variable]))){
	return false;
      }
    }
  }
  return true;
}

bool action_asp_applicable(GlobalOperator &action,
			   std::vector<int> &relaxed_state_values,
			   std::vector<std::vector<bool> > &relaxed_state_primary_variables){
  const std::vector<GlobalCondition>& conditions=action.get_preconditions();
  std::vector<GlobalCondition> secondary_conditions;
  int condition_variable;
  int condition_value;
  std::vector<bool> variable_vector;
  //bool applicable, relaxed_applicable;
  for(size_t i=0;i<conditions.size();i++){
    condition_variable=conditions[i].var;
    condition_value=conditions[i].val;
    variable_vector=relaxed_state_primary_variables[condition_variable];
    if(g_axiom_layers[condition_variable]==-1){		//if it's a primary variable, check whether condition_value is in the vactor
 //     if(not(std::find(variable_vector.begin(), variable_vector.end(), condition_value)!=variable_vector.end())){
      if(not(variable_vector[condition_value])){
#ifdef PRINT_ACTION_APPLICABILITY
	cout << "Action not applicable (because of primary variables)!\n\n";
#endif
	return false;	//if it's not, the action is not applicable
      }
    }else{		
      secondary_conditions.push_back(GlobalCondition(condition_variable, condition_value));
    }
  }
  if(secondary_conditions.size()==0){
    return true;
  }
#ifdef PRINT_ACTION_APPLICABILITY  
  cout << "Secondary conditions size? " << secondary_conditions.size() << endl;
#endif
  //figure out whether the model is satisfiable
  ASPChecker asp_checker;
  bool action_applicable=asp_checker.check_relaxed_state(relaxed_state_primary_variables,secondary_conditions);
#ifdef PRINT_ACTION_APPLICABILITY
  cout << "action_applicable? " << action_applicable << endl;
  if(action_applicable){
    cout << "Found applicable action!\n\n";
  }else{
    cout << "Action not applicable (because of secondary variables)!\n\n";
  }
#endif
  return action_applicable; 
}
      
//Evaluates a vector of conditions:
// - If all conditions are true, returns true (1);
// - If all conditions are true or unknown, returns unknown (2);
// - If there is at least one false condition, returns false (0);

int tvr_evaluate_axiom_conditions(const std::vector<GlobalCondition> &conditions, 
				  const std::vector<int> &relaxed_state_values, 
				  const std::vector<std::vector<bool> > &primary_vars_vector){
  int value=1; //assuming they are all true
  std::vector<bool> variable_vector;
  bool condition_possible;
  bool condition_not_necessary;
  for(unsigned int i=0;i<conditions.size();i++){
    
    //if it's a primary variable
    if(g_axiom_layers[conditions[i].var]==-1){
      variable_vector=primary_vars_vector[conditions[i].var];
      //condition_possible= std::find(variable_vector.begin(), variable_vector.end(), conditions[i].val)!=variable_vector.end();
      condition_possible=variable_vector[conditions[i].val];
      condition_not_necessary=false;
      for(int j=0; j < (int)variable_vector.size(); j++) {
	if(variable_vector[j] && (j != conditions[i].val)) {
	  condition_not_necessary=true;
	  break;
	}
      }
      if(condition_possible && condition_not_necessary){
	value=2;
      }else if(not(condition_possible)){
	return 0;
      }
    
    //if it's a secondary variable
    }else if(relaxed_state_values[conditions[i].var]==g_variable_domain[conditions[i].var]){
      value=2;
    }else if(relaxed_state_values[conditions[i].var]!=conditions[i].val){
      return 0;
    }
  }
  return value;
}

//returns 1 if given 0, returns 0 if given 1
int negation_of(int value){
  if(value==1){
    return 0;
  }else if(value==0){
    return 1;
  }else{
    cout << "Wrong value passed to negation_of!" << endl;
    abort();
  }
  return 2;
}

//new axiom evaluation in a relaxed state

std::vector<int> evaluate_axioms(Relaxed_planning_graph_layer *this_layer, 
				 const std::vector<int> &relaxed_state_values,
				 const std::vector<std::vector<bool> > &primary_vars_vector
				)
{
  
  std::vector<int> new_values=relaxed_state_values; //copy the values
  this_layer->update_values(new_values);
    
  //First, set all of the secondary variables to false.
  for(unsigned int i=0;i<new_values.size();i++){ //WARNING! Not sure whether this is right. Are primary variables always given -1?
    if(g_axiom_layers[i]!=-1){
      new_values[i]=g_default_axiom_values[i];
    }
  }
  
  bool value_changed=false;
  int max_layer=*std::max_element(g_axiom_layers.begin(), g_axiom_layers.end());
  for(int i=0; i<=max_layer; i++){

    do{
      value_changed=false;
      for(unsigned int j=0;j<g_axioms.size();j++){
	const std::vector<GlobalEffect>& effects=g_axioms[j].get_effects();
	for(unsigned int k=0;k<effects.size();k++){
	  if(g_axiom_layers[effects[k].var]==i){
	    
	    /*
	    if(effects[k].var==30){
	      cout << "Evaluating axiom " << j << ". Variable affected " << effects[k].var << " "; 
	      g_axioms[j].dump(); 
	    }
	    */
	    
	    int variable_false=g_default_axiom_values[effects[k].var];
	    int variable_true=negation_of(variable_false);
	 //   cout << "layer:" << i << " axiom: " << j << " ";
	    //g_axioms[j].dump();
	    if(new_values[effects[k].var]!=variable_true){ //If the values has already been assigned true, it remains true. 
	      const std::vector<GlobalCondition>& effect_conditions=effects[k].conditions;
	      int conditions_value=tvr_evaluate_axiom_conditions(effect_conditions,new_values,primary_vars_vector);
	      assert(effects[k].val==variable_true);
	      if(conditions_value==1 && new_values[effects[k].var]==variable_false){
	//	cout << "Changed " << g_variable_name[effects[k].var] << " from " << new_values[effects[k].var] << " to 1.";
		new_values[effects[k].var]=variable_true;
		//if(effects[k].var==30){
		//  cout << "Making it true
		//}
		value_changed=true;
	      }else if(conditions_value==1 && new_values[effects[k].var]==2){
	//	cout << "Changed " << g_variable_name[effects[k].var] << " from " << new_values[effects[k].var] << " to 1.";
		new_values[effects[k].var]=variable_true;
		value_changed=true;
	      }else if(conditions_value==2 && new_values[effects[k].var]==variable_false){
	//	cout << "Changed " << g_variable_name[effects[k].var] << " from " << new_values[effects[k].var] << " to 2.";
		new_values[effects[k].var]=2;
		value_changed=true;
	      }
	    }
	  //  cout << endl;
	  }
	}
      }
    }while(value_changed);
  }

  //abort();
  
  return new_values;
}

Relaxed_planning_graph_layer::Relaxed_planning_graph_layer(Relaxed_planning_graph_layer *previous){
  
  //int i;
  //int *previous_layer_values;

  std::vector<int> new_layer_values(g_variable_name.size());
  std::vector<int> new_layer_used_actions(g_operators.size());
  
  values=new_layer_values;
  used_actions=new_layer_used_actions;
  //used_actions=new int[g_operators.size()];
  
  std::vector<std::vector<bool> > new_layer_vector_values(g_variable_name.size());		//FIXME WARNING TODO vector_values  
  for(unsigned int i=0;i<new_layer_vector_values.size();i++){
    std::vector<bool> v(g_variable_domain[i],false);
    new_layer_vector_values[i]=v;
  }
  primary_values=new_layer_vector_values;

  
  /*
  cout << "Done with creating arrays." <<endl;
  if(previous!=0){
  for(i=0; i<g_variable_name.size(); i++){
    previous_layer_values=previous->values;
    //cout << " "<< i << " " << previous_layer_values[i];
    cout << "Previous , values ... " << previous_layer_values[i] << endl;
    //values[i]=previous_layer->values[i]; //for start, copy the values
  }
  for(i=0; i<g_operators.size(); i++){
    cout << "Previous , actions ... " << previous->used_actions[i] << endl;
    //used_actions[i]=previous_layer->used_actions[i];
    //cout << "used_actions: " << used_actions[i] << " previous_layer->used_actions: " << previous_layer->used_actions[i] << endl;
  }

  
}
  if(previous==0){
    n_layer=0;
  }else{
    cout << "previous n_layer" << previous_layer->n_layer << endl;
    n_layer=(previous_layer->n_layer)+1;
  }
  
  cout << "n_layer is" << n_layer << endl;
  */
  previous_layer=previous;

}

Relaxed_planning_graph_layer::~Relaxed_planning_graph_layer(){
  
}

void Relaxed_planning_graph_layer::make_start_layer(const GlobalState &state){
  for(unsigned int i=0; i<g_variable_name.size(); i++){
    //cout << "Is the problem, like, here?" << state[i] << " " << values[i] << endl;
    values[i]=state[i];  
    primary_values[i][state[i]]=true;	
  }
  
  for(unsigned int i=0;i<g_operators.size(); i++){
    used_actions[i]=0;
  }
  
  //there is no need to evaluate axioms, as this relaxed state is the same as the correspondeing (non-relaxed) state
  
  std::vector<int> new_values=evaluate_axioms(this, values,primary_values);
  values=new_values;
  n_layer=0;
}

void Relaxed_planning_graph_layer::add_new_layer(const std::vector<int> &set_of_actions){
  n_actions_used=0;
  std::vector<int> values_to_modify(g_variable_name.size());
  std::vector<std::vector<bool> > new_primary_vector(g_variable_name.size());
  for(unsigned int i=0;i<g_variable_name.size();i++){
    values_to_modify[i]=0;
    
    //TODO: Not sure whether this is a way of doing a deepcopy.
    new_primary_vector[i]=primary_values[i];
  }
  //int *previous_layer_values;
  //const std::vector<Effect> action_effects;
  //previous_layer=previous;
  //cout << "Copying values" << g_variable_name.size() << endl;
 /* for(i=0; i<g_variable_name.size(); i++){
    previous_layer_values=previous_layer->values;
    //cout << " "<< i << " " << previous_layer_values[i];
    cout << "Copying ... " << previous_layer_values[i] << endl;
    values[i]=previous_layer->values[i]; //for start, copy the values
  }
  for(i=0; i<g_operators.size(); i++){
    cout << "Copying ... " << previous_layer->used_actions[i] << endl;
    used_actions[i]=previous_layer->used_actions[i];
    //cout << "used_actions: " << used_actions[i] << " previous_layer->used_actions: " << previous_layer->used_actions[i] << endl;
  }*/
  //cout << "Done copying values." << endl;
  
  //First, compute effects of actions.
  
  //cout << "Actions:" << endl;
  //cout << "\nNew layer" << endl;
  for(unsigned int i=0;i<g_operators.size(); i++){ 			//for all actions
    assert(!g_operators[i].is_axiom());
    if(used_actions[i]==0 && set_of_actions[i]==1){   	//if unused
      
       /* 
      if(i==379){
      //if(g_operators[i].get_name()=="stepup-tmin2-thro-vego-increase-from-tmin1-l-tmin2-l-t1-l-t2"){
	cout << g_operators[i].get_name() << " Fount it!!" << endl;
	cout << "Tvr applicable? " << action_tvr_applicable(g_operators[i],previous_layer->values,previous_layer->primary_values) << endl;
	cout << "Why?" << endl;
	
	std::vector<int> relaxed_state_values=previous_layer->values;
	std::vector<std::vector<bool> > relaxed_state_primary_variables=previous_layer->primary_values;
	
	const std::vector<GlobalCondition>& conditions=g_operators[i].get_preconditions();
	int condition_variable;
	int condition_value;
	std::vector<bool> variable_vector;
	bool applicable, relaxed_applicable;	
	for(unsigned int j=0;j<conditions.size();j++){
	  condition_variable=conditions[j].var;
	  condition_value=conditions[j].val;
	  variable_vector=relaxed_state_primary_variables[condition_variable];
	  if(g_axiom_layers[condition_variable]==-1){		//if it's a primary variable, check whether condition_value is in the vactor
 //     if(not(std::find(variable_vector.begin(), variable_vector.end(), condition_value)!=variable_vector.end())){
	    cout << g_variable_name[condition_variable] << " Required value:" << condition_value << " State value (bool):" << variable_vector[condition_value] << endl;
	    if(not(variable_vector[condition_value])){
	      //cout << "Action not applicable." << endl;	//if it's not, the action is not applicable
	    }
	  }else{				//if it's a secondary variable, check whether it's corresponding value or unknown
	    applicable = relaxed_state_values[condition_variable]==condition_value;
	    relaxed_applicable = relaxed_state_values[condition_variable]==g_variable_domain[condition_variable];
	    cout << g_variable_name[condition_variable] << " Required value:" << condition_value << " State value:" << relaxed_state_values[condition_variable] << " Applicable? " << applicable << " Relaxed_applicable? " << relaxed_applicable << endl;
	    if(not(applicable || relaxed_applicable)){
    //  if(not((relaxed_state_values[condition_variable]==condition_value) || (relaxed_state_values[var]==g_variable_domain[condition_variable]))){
	      //cout << "Action not applicable." << endl;
	    }
	  }
	}
	//cout << "Action applicable." << endl;
	
	
      } */
      //cout << "Action unused and not an axiom. ";
      //if(g_operators[i].is_tvr_applicable(previous_layer->values)){	//old way, without the primary vector
      if(action_tvr_applicable(g_operators[i],previous_layer->values,previous_layer->primary_values)){	
	//cout << "And it's tvr applicable. ";
	//g_operators[i].dump_noendline();
	//std::vector<Effect> action_effects;
	const std::vector<GlobalEffect>& action_effects = g_operators[i].get_effects();
	//cout << "Got the effects." << endl;
	used_actions[i]=1;
	//cout << " Applying an action. " << g_operators[i].get_name() << endl;
	for(unsigned int j=0;j<action_effects.size();j++){
	  //action_effects[j].dump(); cout << "; ";
	  if(action_effects[j].val != values[action_effects[j].var]){
	    values_to_modify[action_effects[j].var]=g_variable_domain[action_effects[j].var];	//by modifying the values into unknown
	    //new_primary_vector[action_effects[j].var].push_back(action_effects[j].val); //by adding another possible value to the vector of values
	    new_primary_vector[action_effects[j].var][action_effects[j].val]=true;
	  }
	}
	n_actions_used++;
//	cout << " Used an action. n_actions_used is " << n_actions_used << endl;
	//cout << endl;
      }else{
	//const std::vector<Effect> &action_effects=g_operators[i].get_effects();
	//cout << "Not tvr applicable: "; 
	//g_operators[i].dump();
	/*
	for(j=0;j<action_effects.size();j++){
	  action_effects[j].dump();
	} */
	//cout << endl;
      }
    }
  }
  //cout
  for(unsigned int i=0;i<g_variable_name.size();i++){
    if(values_to_modify[i]==g_variable_domain[i]){
      values[i]=g_variable_domain[i];
    }
  }
  
  primary_values=new_primary_vector; //Hopefully that copies everything.
  
  //Secondly, compute the values of the variables affected by axioms.
  //int k;
  //cout << "Here are the axioms (before evaluation)." << endl;
  
  std::vector<int> new_values=evaluate_axioms(this, values,primary_values);
  values=new_values;
  /*
  for(i=0;i<g_axioms.size();i++){
    //cout << "Is tvr applicable? " << g_axioms[i].is_tvr_applicable(this->values);
    //cout << " Conditions ";
    const std::vector<Condition> conditions=g_axioms[i].get_preconditions();
    //cout << " Effects ";
    const std::vector<Effect> effects=g_axioms[i].get_effects();
    for(j=0;j<effects.size();j++){
      //effects[j].dump();
      const std::vector<Condition> effect_conditions=effects[j].get_conditions();
      
      for(int k=0;k<effect_conditions.size();k++){
	effect_conditions[k].dump();
      } 
     cout << " Effect " << j <<" Does tvr fire? " << effects[j].tvr_does_fire(this) << " "; 
     if(g_axioms[i].is_tvr_applicable(this->values) && effects[j].tvr_does_fire(this)){
      if(effects[j].relaxed_does_fire(this->values)){
       values[effects[j].var]=effects[j].val;
      }else{
	if(effects[j].tvr_does_fire(this->values)){
       //if(effects[j].val != values[effects[j].var]){
	    values[effects[j].var]=g_variable_domain[effects[j].var];
	} 
	}
     } 
    }
  g_axioms[i].dump();
  } */
}

void Relaxed_planning_graph_layer::asp_add_new_layer(const std::vector<int> &set_of_actions){
  n_actions_used=0;
  std::vector<int> values_to_modify(g_variable_name.size());
  std::vector<std::vector<bool> > new_primary_vector(g_variable_name.size());
  for(unsigned int i=0;i<g_variable_name.size();i++){
    values_to_modify[i]=0;
    
    //TODO: Not sure whether this is a way of doing a deepcopy.
    new_primary_vector[i]=primary_values[i];
  }
  //int *previous_layer_values;
  //const std::vector<Effect> action_effects;
  //previous_layer=previous;
  //cout << "Copying values" << g_variable_name.size() << endl;
 /* for(i=0; i<g_variable_name.size(); i++){
    previous_layer_values=previous_layer->values;
    //cout << " "<< i << " " << previous_layer_values[i];
    cout << "Copying ... " << previous_layer_values[i] << endl;
    values[i]=previous_layer->values[i]; //for start, copy the values
  }
  for(i=0; i<g_operators.size(); i++){
    cout << "Copying ... " << previous_layer->used_actions[i] << endl;
    used_actions[i]=previous_layer->used_actions[i];
    //cout << "used_actions: " << used_actions[i] << " previous_layer->used_actions: " << previous_layer->used_actions[i] << endl;
  }*/
  //cout << "Done copying values." << endl;
  
  //First, compute effects of actions.
  
  //cout << "Actions:" << endl;
  //cout << "\nNew layer" << endl;
  for(unsigned int i=0;i<g_operators.size(); i++){ 			//for all actions
    assert(!g_operators[i].is_axiom());
    if(used_actions[i]==0 && set_of_actions[i]==1){   	//if unused
      
      //if(action_tvr_applicable(g_operators[i],previous_layer->values,previous_layer->primary_values)){	
#ifdef PRINT_ACTION_APPLICABILITY
      g_operators[i].dump();
#endif
      if(action_asp_applicable(g_operators[i],previous_layer->values,previous_layer->primary_values)){
	//cout << "And it's tvr applicable. ";
	//g_operators[i].dump();
	//std::vector<Effect> action_effects;
	const std::vector<GlobalEffect>& action_effects=g_operators[i].get_effects();
	//cout << "Got the effects." << endl;
	used_actions[i]=1;
	//cout << " Applying an action. " << g_operators[i].get_name() << endl;
	for(unsigned int j=0;j<action_effects.size();j++){
	  //action_effects[j].dump(); cout << "; ";
	  if(action_effects[j].val != values[action_effects[j].var]){
	    values_to_modify[action_effects[j].var]=g_variable_domain[action_effects[j].var];	//by modifying the values into unknown
	    //new_primary_vector[action_effects[j].var].push_back(action_effects[j].val); //by adding another possible value to the vector of values
	    new_primary_vector[action_effects[j].var][action_effects[j].val]=true;
	  }
	}
	n_actions_used++;
//	cout << " Used an action. n_actions_used is " << n_actions_used << endl;
	//cout << endl;
      }else{
	//const std::vector<Effect> &action_effects=g_operators[i].get_effects();
	//cout << "Not tvr applicable: "; 
	//g_operators[i].dump();
	/*
	for(j=0;j<action_effects.size();j++){
	  action_effects[j].dump();
	} */
	//cout << endl;
      }
    }
  }
  //cout
  for(unsigned int i=0;i<g_variable_name.size();i++){
    if(values_to_modify[i]==g_variable_domain[i]){
      values[i]=g_variable_domain[i];
    }
  }
  
  primary_values=new_primary_vector; //Hopefully that copies everything.
  
  //Secondly, compute the values of the variables affected by axioms.
  //int k;
  //cout << "Here are the axioms (before evaluation)." << endl;
  
  //std::vector<int> new_values=evaluate_axioms(this, values,primary_values);
  //values=new_values;
}

void Relaxed_planning_graph_layer::set_n_layer(int n){
  //cout << "n is " << n << endl;
  n_layer=n;
  //cout << "n_layer is " << n_layer << endl;
}

int Relaxed_planning_graph_layer::get_n_layer(){
  return n_layer;
}

//Simlar to above, except, it uses a single action.
//Creates a new layer and outputs it.

//WARNING: This method does not work!!
void Relaxed_planning_graph_layer::use_single_action(int &action){
    
  n_actions_used=0;
  std::vector<int> values_to_modify(g_variable_domain.size(),0);
  std::vector<std::vector<bool> > new_primary_vector(g_variable_name.size());
  for(unsigned int i=0;i<g_variable_name.size();i++){
    values_to_modify[i]=0;
    
    //TODO: Not sure whether this is a way of doing a deepcopy.
    new_primary_vector[i]=primary_values[i];
  }
  /*
  for(i=0;i<g_variable_name.size();i++){
    values_to_modify[i]=0;
  }*/
  
  //for(i=0;i<g_operators.size(); i++){ 			//for all actions
  if(used_actions[action]==0 && !g_operators[action].is_axiom()){   	//if unused
      //cout << "Action unused and not an axiom." << endl;
      //if(g_operators[action].is_tvr_applicable(previous_layer->values)){	//use it
      if(action_tvr_applicable(g_operators[action],previous_layer->values,previous_layer->primary_values)){
	//cout << "And it's tvr applicable." << endl;
	//g_operators[action].dump();
	//std::vector<Effect> action_effects;
	const std::vector<GlobalEffect> & action_effects = g_operators[action].get_effects();
	//cout << "Got the effects." << endl;
	used_actions[action]=1;
	//cout << "Applying an action. Effects: ";
	for(unsigned int j=0;j<action_effects.size();j++){
	  //action_effects[j].dump();
	  if(action_effects[j].val != values[action_effects[j].var]){
	    //cout << "\nModifying " << endl;
	    //cout << "action_effects[j].var " << action_effects[j].var << endl;
	    new_primary_vector[action_effects[j].var][action_effects[j].val]=true;
	    //new_primary_vector[action_effects[j].var].push_back(action_effects[j].val);
	    values_to_modify[action_effects[j].var]=g_variable_domain[action_effects[j].var];	//by modifying the values into unknown
	    n_actions_used++;
	    //cout << " Used an action. n_actions_used is " << n_actions_used << endl;
	  }
	}
	//cout << endl;
      }
    }
  
  for(unsigned int i=0;i<g_variable_name.size();i++){
     if(values_to_modify[i]==g_variable_domain[i]){
      values[i]=g_variable_domain[i];
    }
  }
  
  //Secondly, compute the values of the variables affected by axioms.
  std::vector<int> new_values=evaluate_axioms(this, values, primary_values);
  values=new_values;
  
}

void Relaxed_planning_graph_layer::print_layer() {
  for(unsigned int i=0; i<g_variable_name.size(); i++){
    if(g_axiom_layers[i]==-1){
      cout << g_variable_name[i] << " : " << values[i] << " g_axiom_layers : " << g_axiom_layers[i] << " ";
      cout << primary_values[i];
      cout << endl;
    }

  }
  for(unsigned int i=0;i<g_operators.size(); i++){
    //g_operators[i].dump_noendline();
    cout << g_operators[i].get_name() << " : " << used_actions[i] << endl; 
  }
}

void Relaxed_planning_graph_layer::string_layer() {
  cout << "This does not work yet." << endl;
}

//checks whether goals have been reached 
bool Relaxed_planning_graph_layer::applicable_actions_remaining() {
  return n_actions_used > 0;
}

//checks whether goals have been reached 
bool Relaxed_planning_graph_layer::goals_reached(){
  //cout << "Goal reached? " << endl;
  int var, value;
  for (unsigned int i = 0; i < g_goal.size(); i++) {
    var = g_goal[i].first; 
    value = g_goal[i].second;
    //cout << "Goal is " << var << ":" << value << " and in this layer " << var << ":" << values[var] << << endl;
    if(g_axiom_layers[var]==-1){ //if it's a primary variable
      if(not(primary_values[var][value])){
	return false;
      }
	
    }else{ //if it's a secondary variable
      if(!(values[var]==g_variable_domain[var] || values[var]==value)){
	return false;
      }
    }
  }
  return true;
}

//checks whether goals have been reached 
bool Relaxed_planning_graph_layer::asp_goals_reached(){
  //cout << "Goal reached? " << endl;
  
  // need to convert the goal into a GlobalCondition vector; also
  // split it into primary/secondary parts:
  
//  std::vector<GlobalCondition> primary_goal;
  int gvar, gval;
  std::vector<GlobalCondition> secondary_goal;
  for (unsigned int i = 0; i < g_goal.size(); i++) {
    gvar = g_goal[i].first;
    gval = g_goal[i].second;
    if (g_axiom_layers[gvar] < 0){ //if it's a primary variable
      if(not(primary_values[gvar][gval])){ //check whether it's satisfied
	return false; //if it isn't return false
      }
      }else{ //otherwise, make it part of a secondary goal
	secondary_goal.push_back(GlobalCondition(gvar, gval));
    }
  }
  ASPChecker asp_checker;
  bool secondary_goal_satisfied=asp_checker.check_relaxed_state(primary_values,secondary_goal); //figure out whether the model is satisfiable
#ifdef TRACE_PRINT_API_CALLS
  cout << "secondary_goal_satisfied? " << secondary_goal_satisfied << "\n";
#endif
  return secondary_goal_satisfied; 
}

Relaxed_planning_graph_layer relaxed_reachable_with_set_of_actions(const GlobalState &state, const std::vector<int> &set_of_actions){

    int n_layer=0;
    bool r_reachable=false;
    
    // - Just printing some stuff out.
//    size_t i;
    
    /*
    cout << "Variables in this state:"<< endl; 
    for (i = 0; i < g_variable_name.size(); i++){
      cout << g_variable_name[i] << " : " << state[i] << " g_axiom_layers : " << g_axiom_layers[i] << " ";
      cout << endl;      
    }
    */
    //cout << "Set of actions? " << set_of_actions << endl;
    //std::vector<Effect> action_effects;
    /*
    cout << "Operators?" << endl;
    for(i=0;i<g_operators.size();i++){
      g_operators[i].dump();
    }
    
    cout << "Axioms?" << endl;
    
    for(i=0;i<g_axioms.size();i++){
      //cout << "Hey, an axiom." << endl;
      g_axioms[i].dump();
    } */
    
        
 //   cout << "Creating first layer." << endl;
    Relaxed_planning_graph_layer start_layer(0);
    Relaxed_planning_graph_layer *new_layer_p=&start_layer;
    start_layer.n_layer=n_layer;
    start_layer.make_start_layer(state);
    r_reachable=start_layer.goals_reached();
    bool applicable_actions_still_remaining=start_layer.applicable_actions_remaining();
    
    //cout << "\n\n\nStart layer. \n\n"; 
    //start_layer.print_layer();
    //cout << "Goals reached? " << start_layer.goals_reached() << "\n\n\n\n\n";

    if(r_reachable){
      return start_layer;
    }
    
    std::vector<int> new_layer_values=start_layer.values;
    std::vector<int> new_layer_applied_actions=start_layer.used_actions;
    std::vector<std::vector<bool> > new_layer_vector_primary=start_layer.primary_values;
    
    std::vector<int> current_layer_values=start_layer.values;
    std::vector<int> current_applied_actions=start_layer.used_actions;
    std::vector<std::vector<bool> > current_vector_primary=start_layer.primary_values;
    
    //Relaxed_planning_graph_layer next_layer(new_layer_p);
    //cout << "Initial layer.\n";
    do{
      
      n_layer++;
      //cout << "n_layer is " << n_layer << endl;

      current_layer_values=new_layer_values;
      current_applied_actions=new_layer_applied_actions;
      current_vector_primary=new_layer_vector_primary;
      //cout << "new_layer_values " << new_layer_values << " current_layer_values " << current_layer_values << endl;
      /*
      for(i=0; i<g_variable_name.size(); i++){
	current_layer_values[i]=new_layer_values[i];
      }
      for(i=0; i<g_operators.size(); i++){
	current_applied_actions[i]=new_layer_applied_actions[i];
      }
      */
      //cout << "Creating a new layer." << endl;
      Relaxed_planning_graph_layer next_layer(new_layer_p);
      next_layer.set_n_layer(n_layer);
      
      next_layer.values=current_layer_values;
      next_layer.used_actions=current_applied_actions;
      next_layer.primary_values=current_vector_primary;
      /*
      for(i=0; i<g_variable_name.size(); i++){
	next_layer.values[i]=current_layer_values[i];
      }
      for(i=0; i<g_operators.size(); i++){
	next_layer.used_actions[i]=current_applied_actions[i];
      } */
            
      new_layer_p=&next_layer;
      
    //  cout << "Adding a new layer" << endl;
      next_layer.add_new_layer(set_of_actions);
      r_reachable=next_layer.goals_reached();
      applicable_actions_still_remaining=next_layer.applicable_actions_remaining();
      
      
      //cout << "\n\n\nNew layer. \n\n"; 
      //next_layer.print_layer();
      //cout << "Goals reached? " << r_reachable << "\n\n\n\n\n";
      
      
      //cout << "next_layer.values " << next_layer.values << endl;
      //cout << "next_layer.used_actions " << next_layer.used_actions << endl;
      
      new_layer_values=next_layer.values;
      new_layer_applied_actions=next_layer.used_actions;
      new_layer_vector_primary=next_layer.primary_values;
      
      //cout << "new_layer_values " << new_layer_values << endl;
      //cout << "new_layer_used_actions " << new_layer_applied_actions << endl;
      
      if(r_reachable || !applicable_actions_still_remaining){
	return next_layer;
      }
      
      /*
      if(n_layer>0){
	abort();
      }
      */
      
    }while(!r_reachable && applicable_actions_still_remaining);
    
    //next_layer.set_n_layer(n_layer);
    
  //  cout << "Is the goal relaxed reachable with the set of actions? " << r_reachable << endl;
  //  cout << "n_layer of the final relaxed state is " << n_layer << " " << next_layer.get_n_layer() << endl;
    
    return start_layer;
}

Relaxed_planning_graph_layer asp_relaxed_reachable_with_set_of_actions(const GlobalState &state, const std::vector<int> &set_of_actions){

    int n_layer=0;
    bool r_reachable=false;
//    size_t i;
            
  // cout << "Creating first layer." << endl;
    Relaxed_planning_graph_layer start_layer(0);
    Relaxed_planning_graph_layer *new_layer_p=&start_layer;
    start_layer.n_layer=n_layer;
    start_layer.make_start_layer(state);
    r_reachable=start_layer.asp_goals_reached();
    bool applicable_actions_still_remaining=start_layer.applicable_actions_remaining();
  //  cout << "r_reachable? " << r_reachable << endl;
    //abort();
    //assert(0);
#ifdef PRINT_RELAXED_PLANNING_GRAPH    
    cout << "\n\n\nStart layer. \n\n"; 
    start_layer.print_layer();
    cout << "Goals reached? " << start_layer.goals_reached() << "\n\n\n\n\n";
#endif
    if(r_reachable){
      return start_layer;
    }
    
    std::vector<int> new_layer_values=start_layer.values; //NOTE Not needed.
    std::vector<int> new_layer_applied_actions=start_layer.used_actions;
    std::vector<std::vector<bool> > new_layer_vector_primary=start_layer.primary_values;
    
    std::vector<int> current_layer_values=start_layer.values; //NOTE Not needed.
    std::vector<int> current_applied_actions=start_layer.used_actions;
    std::vector<std::vector<bool> > current_vector_primary=start_layer.primary_values;
    
    //Relaxed_planning_graph_layer next_layer(new_layer_p);
    //cout << "Initial layer.\n";
    do{
      
      n_layer++;
#ifdef PRINT_RELAXED_PLANNING_GRAPH
      cout << "n_layer is " << n_layer << endl;
#endif
      current_layer_values=new_layer_values;
      current_applied_actions=new_layer_applied_actions;
      current_vector_primary=new_layer_vector_primary;
      
      //cout << "new_layer_values " << new_layer_values << " current_layer_values " << current_layer_values << endl;
      /*
      for(i=0; i<g_variable_name.size(); i++){
	current_layer_values[i]=new_layer_values[i];
      }
      for(i=0; i<g_operators.size(); i++){
	current_applied_actions[i]=new_layer_applied_actions[i];
      }
      */
      //cout << "Creating a new layer." << endl;
      
      Relaxed_planning_graph_layer next_layer(new_layer_p);
      next_layer.set_n_layer(n_layer);
      
      next_layer.values=current_layer_values;  //NOTE Not needed.
      next_layer.used_actions=current_applied_actions;
      next_layer.primary_values=current_vector_primary;
      /*
      for(i=0; i<g_variable_name.size(); i++){
	next_layer.values[i]=current_layer_values[i];
      }
      for(i=0; i<g_operators.size(); i++){
	next_layer.used_actions[i]=current_applied_actions[i];
      } */
            
      new_layer_p=&next_layer;
      
    //  cout << "Adding a new layer" << endl;
      next_layer.asp_add_new_layer(set_of_actions);
      r_reachable=next_layer.asp_goals_reached();
      applicable_actions_still_remaining=next_layer.applicable_actions_remaining();
#ifdef PRINT_RELAXED_PLANNING_GRAPH
      cout << "\n\n\nNew layer. \n\n"; 
      next_layer.print_layer();
      cout << "Goals reached? " << r_reachable << "\n\n\n\n\n";
#endif      
      //cout << "next_layer.values " << next_layer.values << endl;
      //cout << "next_layer.used_actions " << next_layer.used_actions << endl;
      
      new_layer_values=next_layer.values;  //NOTE Not needed.
      new_layer_applied_actions=next_layer.used_actions;
      new_layer_vector_primary=next_layer.primary_values;
      
      //cout << "new_layer_vector_primary " << new_layer_vector_primary << endl;
      //cout << "new_layer_used_actions " << new_layer_applied_actions << endl;
      
      if(r_reachable || !applicable_actions_still_remaining){
	return next_layer;
      }
            
    }while(!r_reachable && applicable_actions_still_remaining);
    
    //next_layer.set_n_layer(n_layer);
    
  //  cout << "Is the goal relaxed reachable with the set of actions? " << r_reachable << endl;
  //  cout << "n_layer of the final relaxed state is " << n_layer << " " << next_layer.get_n_layer() << endl;
    
    return start_layer;
}



bool goals_relaxed_reachable(const GlobalState &state, const std::vector<int> &set_of_actions){
#ifdef USE_TVR
  Relaxed_planning_graph_layer final_layer=relaxed_reachable_with_set_of_actions(state, set_of_actions);
  return final_layer.goals_reached();
#else
  Relaxed_planning_graph_layer final_layer=asp_relaxed_reachable_with_set_of_actions(state, set_of_actions);
  return final_layer.asp_goals_reached();
#endif
}

std::vector<int> find_complement_set(const std::vector<int> &set_A){
  std::vector<int> complement(g_operators.size());
  for(unsigned int i=0;i<g_operators.size();i++){
    if(set_A[i]==0){
      complement[i]=1;
    }else{
      complement[i]=0;
    }
  }
  return complement;
}

//TODO This version of new_landmark works, but it keeps rebuilding the graph instead of reusing it.
std::vector<int> new_landmark(const GlobalState &state, const std::vector<int> &set_of_actions_A){
  
  //For now, this function builds the graph from the start.
  
  //cout << "Starting new_landmark method." << endl;
  std::vector<int> set_A=set_of_actions_A;
  std::vector<int> actions_to_try=set_A;
  //cout << "set_A at start: " << set_A << endl;
#ifdef USE_TVR
  Relaxed_planning_graph_layer final_layer=relaxed_reachable_with_set_of_actions(state, set_A);
  bool r_reachable=final_layer.goals_reached();
#else
  Relaxed_planning_graph_layer final_layer=asp_relaxed_reachable_with_set_of_actions(state, set_A);
  bool r_reachable=final_layer.asp_goals_reached();
#endif

  //cout << "Finding the complement of A" << endl;
  std::vector<int> complement_A=find_complement_set(set_A);
  //cout << complement_A << endl;
  
  for(unsigned int i=0;i<g_operators.size();i++){
    if(complement_A[i]==1){
            
      //Try building a new layer.
      //cout << "Using a single action. " << i << endl;
      actions_to_try[i]=1;
      
      r_reachable=goals_relaxed_reachable(state, actions_to_try);
      
      //If the goal is not reached, add the used action to the set of actions.
      //cout << "Goals reachd? " << r_reachable << endl;      
      if(!r_reachable){
	
	set_A[i]=1;
	//cout << "set_A " << set_A << endl;
      }else{
	actions_to_try[i]=0;
      }
    }
  }

  return find_complement_set(set_A);
}

std::vector<int> union_of_landmarks(const std::vector< std::vector <int> > &landmarks){

  std::vector<int> all_landmarks(g_operators.size(), 0);

    /*
  cout << "Finding the union of landmarks." << endl;
  for(i=0;i<landmarks.size();i++){
    cout << landmarks[i] << endl;
  }*/
  
  for(unsigned int i=0;i<g_operators.size();i++){
    for(unsigned int j=0;j<landmarks.size();j++){
      if(landmarks[j][i]==1){
	all_landmarks[i]=1;
	break;
      }
    }
  }
  
  //cout << "all_landmarks: " << all_landmarks << endl;
  
  return all_landmarks;
}

std::vector<std::vector <int> > ThreeValueHeuristic::compute_landmarks(const GlobalState &state, const std::vector< std::vector<int> > &collectionL){
  
  /*Landmark is a vector of int, of the same length as the number of operators. 
  If the 
  */
  //int i,j;
  int k;
  bool goal_reachable=false;
  std::vector<std::vector <int> > landmarks=collectionL;
  std::vector<int> all_landmarks(g_operators.size());
  
  //These are to be reused.
  /*
  for(i=0;i<collectionL.size();i++){
    cout << "Landmark. " << i << endl;
    for(j=0;j<g_operators.size();j++){
      cout << collectionL[i][j] << " " << endl;
    }
    cout << endl;
  }
  */
  
  k=0; 
  while(!goal_reachable){
    k++;
    
    //cout << "Computing the landmarks " << k << endl;
    all_landmarks=union_of_landmarks(landmarks);
    for(unsigned int i=0;i<landmarks.size();i++){
      //cout << landmarks[i] << endl;
    }
    //cout << "Union of landmarks: " << all_landmarks << endl;
    //cout << "Using relaxed_reachable method." << endl;
    //goal_reachable=relaxed_reachable(state,all_landmarks);
#ifdef USE_TVR
    Relaxed_planning_graph_layer final_layer=relaxed_reachable_with_set_of_actions(state, all_landmarks);
    goal_reachable=final_layer.goals_reached();
#else
    Relaxed_planning_graph_layer final_layer=asp_relaxed_reachable_with_set_of_actions(state, all_landmarks);
    goal_reachable=final_layer.asp_goals_reached();
#endif

    //cout << "Has the goal been reached? " << goal_reachable << endl;
    if(goal_reachable){
      break;
    }
  //  cout << "..................................................................................Using new_landmark method." << endl;
    landmarks.push_back(new_landmark(state,all_landmarks));
    
    //if(k==1){
      //abort();
    //}
  }
  return landmarks;
}

int ThreeValueHeuristic::compute_lmcut_equivalent(const GlobalState &state){
  std::vector<std::vector<int> > collectionL;
  collectionL.clear();
  
  std::vector<int> all_actions(g_operators.size(), 1);
  if(!goals_relaxed_reachable(state, all_actions)){
    return DEAD_END; //return infinity;
  }
  
  std::vector<std::vector<int> > landmarks=compute_landmarks(state, collectionL); 
    
  return landmarks.size(); 
}

std::vector<int> find_min_cost_hitting_set
(std::vector< std::vector <int> > &landmarks)
{
  if ((0 < landmarks.size()) || (landmarks.size() <= 0))
    assert(0);

#ifdef USE_GUROBI
    int i,j;
    GRBEnv env = GRBEnv();
    GRBModel model = GRBModel(env);
    model.getEnv().set(GRB_IntParam_OutputFlag, 0);
    
    // Create variables
    std::vector<GRBVar> variables(g_operators.size());
    for(i=0;i<g_operators.size();i++){
      variables[i]=model.addVar(0.0, 1.0, 0.0, GRB_BINARY);
    }

    // Integrate new variables
    model.update();

    // Set objective
    GRBLinExpr obj=0;
    for(i=0;i<g_operators.size();i++){
      obj += variables[i]*g_operators[i].get_cost();
    }
    model.setObjective(obj, GRB_MINIMIZE);

    // Add constraints
    std::vector<GRBLinExpr> constr(landmarks.size());
    for(i=0;i<landmarks.size();i++){
      constr[i]=0;
      for(j=0;j<g_operators.size();j++){
	if(landmarks[i][j]==1){
	  constr[i] += variables[j];
	}
      }
      model.addConstr( constr[i] >= 1);
    }

    // Optimize model
    model.update();
    model.optimize();
    
    std::vector<int> minimum_cost_hitting_set(g_operators.size(), 0);
    for(i=0; i<g_operators.size();i++){
	minimum_cost_hitting_set[i]=variables[i].get(GRB_DoubleAttr_X );
    } 

    return minimum_cost_hitting_set;
#else
    assert(0);
#endif
    std::vector<int> minimum_cost_hitting_set(g_operators.size(), 0);
    return minimum_cost_hitting_set;
}

int ThreeValueHeuristic::compute_hplus(const GlobalState &state, const std::vector<std::vector<int> > &collectionL){
  
  std::vector<int> all_actions(g_operators.size(), 1);
  if(!goals_relaxed_reachable(state, all_actions)){
    return DEAD_END; //return infinity;
  }
  
  //initially, min_cost_hitting_set is an empty set
  bool goal_reachable=false;
  std::vector<std::vector<int> > landmarks=collectionL; 
  std::vector<int> min_cost_hitting_set(g_operators.size());
  for(unsigned int i=0; i<g_operators.size(); i++){
    min_cost_hitting_set[i]=0;
  }
  
  while(!goal_reachable){
    goal_reachable=goals_relaxed_reachable(state,min_cost_hitting_set);
    if(goal_reachable){
      break;
    }
    landmarks.push_back(new_landmark(state,min_cost_hitting_set));
    min_cost_hitting_set=find_min_cost_hitting_set(landmarks);
  }
  int total_cost=0;
  for(unsigned int i=0;i<g_operators.size();i++){
    if(min_cost_hitting_set[i]==1){
      total_cost += g_operators[i].get_cost();
    }
  }
  
  /*
  for(i=0;i<landmarks.size();i++){
    cout << "Landmark " << i+1 << " " << landmarks[i] << endl;
  }
  cout << "min_cost_hitting_set " << min_cost_hitting_set << " cost " << total_cost << endl;
  */
  
  return total_cost;
}

int ThreeValueHeuristic::compute_hmax(const GlobalState &state) {
  std::vector<int> all_actions(g_operators.size(), 1);
#ifdef USE_TVR
  Relaxed_planning_graph_layer relaxed_state=relaxed_reachable_with_set_of_actions(state, all_actions);
  if(relaxed_state.goals_reached()){
    // cout << "hmax is " << relaxed_state.get_n_layer() << endl;
    return relaxed_state.get_n_layer();
  }else{
    // cout << "Returning DEAD_END" << endl;
    return DEAD_END;
  }
#else
  Relaxed_planning_graph_layer relaxed_state=asp_relaxed_reachable_with_set_of_actions(state, all_actions);
  //cout << "Returning " << relaxed_state.n_layer << endl;
  if(relaxed_state.asp_goals_reached()){
    //   cout << "hmax is " << relaxed_state.get_n_layer() << endl;
    //abort();
    return relaxed_state.get_n_layer();
  }else{
    // cout << "Returning DEAD_END" << endl;
    return DEAD_END;
  }
#endif
}

int ThreeValueHeuristic::compute_heuristic(const GlobalState &state) {
    
    //variable_details();
  
    //abort();
  
    //cout << "testing clasp" << endl;
    //test_clasp();
    //abort();
    
    std::vector<int> all_actions(g_operators.size(), 1);
    std::vector<int> empty_set(g_operators.size(), 0);

    std::vector<std::vector<int> > empty_collection(0);
    
    if(h_used==0){
	  int hmax=compute_hmax(state);
//	  cout << "hmax is " << hmax << endl;
	  //abort();
	  return hmax;
    }else if(h_used==1){
          int lmcut_equivalent=compute_lmcut_equivalent(state);
	  //int hmax=compute_hmax(state);
	  //cout << "Lm-cut equivalent is " << lmcut_equivalent << endl;
	  //abort();
	  return lmcut_equivalent;
    }else if(h_used==2){
          int hplus=compute_hplus(state,empty_collection);
	  //cout << "hplus is " << hplus << endl;
	  //abort();
	  return hplus;
    }
    return 1;
}

static Heuristic *_parse(OptionParser &parser) {
    parser.document_synopsis("Three value relaxed reachability heuristic", "");
    parser.document_language_support("action costs", "supported");
    parser.document_language_support("conditional effects", "supported");
    parser.document_language_support("axioms", "supported");
    parser.document_property("admissible", "yes");
    parser.document_property("consistent", "yes");
    parser.document_property("safe", "yes");
    parser.document_property("preferred operators", "no");

    vector<string> heuristic_used;
    heuristic_used.push_back("hmax");
    heuristic_used.push_back("lmcut_equivalent");
    heuristic_used.push_back("hplus");
    parser.add_enum_option("heuristic_used", heuristic_used,
                           "Choose between hmax, lmcut_equivalent and hplus"
                           , "hmax");
    
    Heuristic::add_options_to_parser(parser);
    Options opts = parser.parse();
    if (parser.dry_run())
        return 0;
    else
        return new ThreeValueHeuristic(opts);
}

static Plugin<Heuristic> _plugin("threevalue", _parse);


