#ifndef PDB_WITH_AXIOMS
#define PDB_WITH_AXIOMS

#include "heuristic.h"
#include "pdbs/pdb_heuristic.h"
#include "global_operator.h"

class PDBWithAxioms : public PDBHeuristic {
  /* We only need to override a few methods from PDBHeuristic.
     In particular, the compute_heuristic method should work without
     change, since the changes are all in the computation of the
     PDB, not the lookup. However, other methods of PDBHeuristic
     (particularly internal, non-public ones) may not work correctly.
  */
  void create_pdb(const std::vector<int> &operator_costs = std::vector<int>());

  /* Set the pattern and compute the PDB. The pattern must consist of
   primary (non-derived) variables only. The same assumptions as for
   PDBHeuristic must hold (the pattern (passed via Options) is
   sorted, contains no duplicates and is small enough so that the
   number of abstract states is below numeric_limits<int>::max()).
  */
  void set_pattern(const std::vector<int> &pattern,
		   const std::vector<int> &operator_costs = std::vector<int>());

  size_t hash_abstract(const std::vector<int> & state) const;
  void print_abstract_state(const std::vector<int> & state) const;
  bool abs_applicable_primary(const std::vector<GlobalCondition>& pre,
			      const std::vector<int> & state) const;
  void get_secondary_condition(const std::vector<GlobalCondition>& cond,
			       std::vector<GlobalCondition>& sec_cond) const;
  int find_in_pattern(int var) const;

 public:
  PDBWithAxioms(const Options &opts,
		bool dump,
		const std::vector<int> &operator_costs);
  virtual ~PDBWithAxioms();

};

#endif
