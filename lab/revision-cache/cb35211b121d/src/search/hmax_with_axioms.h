#ifndef MAX_HEURISTIC_H
#define MAX_HEURISTIC_H

#include "heuristic.h"
#include "global_operator.h"
#include "priority_queue.h"
#include <cassert>
#include <vector>

// forward declare so we don't have to include all the clasp stuff
class ASPChecker;

class HMaxWithAxioms : public Heuristic
{
 protected:
  ASPChecker* checker;
  bool use_tvr;
  int n_calls;

  std::vector< std::vector<bool> > rstate;
  std::vector< std::vector<bool> > remaining_effects;
  std::vector<GlobalCondition> goal_condition;

  std::vector< std::vector<GlobalEffect> > layers;

  virtual void initialize();
  virtual int compute_heuristic(const GlobalState &state);

  bool evaluate(const std::vector<GlobalCondition>& condition,
		const std::vector< std::vector<bool> >& rstate);
  int evaluate3(const std::vector<GlobalCondition>& condition,
		const std::vector< std::vector<bool> >& rstate);
  void enqueue_effects(int current_cost,
		       const std::vector< std::vector<bool> >& rstate,
		       std::vector< std::vector<bool> >& remaining_effects,
		       BucketQueue<GlobalCondition>& queue);
  void find_axioms_in_layer(int layer,
			    std::vector<GlobalEffect>& axioms_in_layer);
  void apply_axioms(const std::vector<GlobalEffect> axioms,
		    std::vector< std::vector<bool> >& rstate);
  void update_derived_variables();

public:
  HMaxWithAxioms(const Options &options);
  ~HMaxWithAxioms();
  virtual bool dead_ends_are_reliable() const;
};

#endif
