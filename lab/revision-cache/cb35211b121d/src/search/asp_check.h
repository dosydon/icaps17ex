#ifndef ASP_CHECK_H
#define ASP_CHECK_H

#include "global_operator.h"
#include "timer.h"
#include <vector>

class ASPChecker {
  std::vector<int> first_fact;
  int n_calls;
  Timer asp_time_total;
  Timer asp_time_solve;

 public:
  ASPChecker();
  ~ASPChecker();

  // check if requirements are satisfiable in a projection.
  // - pattern is the set of projected-on variables, whose values
  //   are taken from the state; pattern variables can only be
  //   primary (non-derived).
  // - reqs is the list (conjunction) of derived variables required
  //   to be true.
  bool check_projection(const std::vector<int> & pattern,
			const std::vector<int> & astate,
			const std::vector<GlobalCondition> & reqs);

  // check if requirements are satisfiable in a relaxed state.
  // - rstate is the relaxed state of the primary variables (a
  //   vector of booleans representing possible values for each
  //   primary variable).
  // NOTE HOWEVER: The size of the rstate vector is the total number
  // of variables (primary and secondary), and it is indexed by global
  // variable number. The positions in the vector that correspond to
  // derived variables are simply unused.
  // - reqs is the list (conjunction) of derived variables required
  //   to be true.
  bool check_relaxed_state(const std::vector< std::vector<bool> > & rstate,
			   const std::vector<GlobalCondition> & reqs);

  int number_of_calls() { return n_calls; };
  double total_time() { return asp_time_total(); };
  double solve_time() { return asp_time_solve(); };
};

#endif
