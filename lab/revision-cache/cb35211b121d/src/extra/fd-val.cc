
#include "state_registry.h"
#include "axioms.h"
#include "successor_generator.h"
#include "globals.h"
#include "global_operator.h"
#include "global_state.h"
#include "utilities.h"

#include <iostream>
#include <cstring>
using namespace std;

int main(int argc, const char **argv) {
  int verbosity = argc - 1;

  bool poly_time_method = false;
  istream &in = cin;
  //in >> poly_time_method;
  read_everything(in);

  if (verbosity > 2) {
    cout << "successor generator:" << endl;
    g_successor_generator->dump();
  }

  StateRegistry *state_reg = g_state_registry;

  GlobalState my_state = state_reg->get_initial_state();
  if (verbosity > 1) {
    cout << "initial state:" << endl;
    //my_state.dump_fdr();
    my_state.dump_pddl();
  }

  while (!in.eof()) {
    //cout << "reading next operator..." << endl;
    char buf[1000];
    in.getline(buf, 1000, '\n');

    if (strlen(buf) > 2) {
      string next_op_name = string(&buf[1], strlen(buf) - 2);
      cout << "next operator: " << next_op_name << endl;
      std::vector<const GlobalOperator *> app;
      app.clear();
      g_successor_generator->generate_applicable_ops(my_state, app);
      //cout << app.size() << " applicable operators:" << endl;
      int next_op_index = -1;
      for (unsigned int i = 0; (i < app.size()) && (next_op_index < 0); i++)
	if (app[i]->get_name() == next_op_name)
	  next_op_index = i;
      if (next_op_index < 0) {
	cout << "error: next operator not found in applicable" << endl;
	cout << "current state is:" << endl;
	my_state.dump_fdr();
	//my_state.dump_pddl();
	cout << "applicable operators are:" << endl;
	for (unsigned int i = 0; i < app.size(); i++)
	  cout << app[i]->get_name() << endl;
	return 1;
      }
      else {
	cout << "next operator found at " << next_op_index << endl;
      }
      if (verbosity > 0) {
	cout << "operator #" << next_op_index << ":" << endl;
	const std::vector<GlobalCondition>& pre =
	  app[next_op_index]->get_preconditions();
	cout << pre.size() << " preconditions:" << endl;
	for (int i = 0; i < pre.size(); i++) {
	  int var = pre[i].var;
	  int val = pre[i].val;
	  cout << " " << g_variable_name[var] << " = " << val
	       << " (state: " << (my_state)[var] << ")" << endl;
	}
	const std::vector<GlobalEffect>& eff =
	  app[next_op_index]->get_effects();
	cout << eff.size() << " effects:" << endl;
	for (int i = 0; i < eff.size(); i++) {
	  int var = eff[i].var;
	  int post_val = eff[i].val;
	  const std::vector<GlobalCondition>& econd = eff[i].conditions;
	  cout << " " << g_variable_name[var] << " <- " << post_val
	       << " (state: " << my_state[var] << ")" << endl;
	  for (int j = 0; j < econd.size(); j++) {
	    int ec_var = econd[j].var;
	    int ec_val = econd[j].val;
	    cout << "  if " << g_variable_name[ec_var] << " = " << ec_val
		 << " (state: " << my_state[ec_var] << ")" << endl;
	  }
	}
      }
      const GlobalState& new_state =
	state_reg->get_successor_state(my_state, *(app[next_op_index]));
      bool any_change = false;
      for (int i = 0; i < g_variable_name.size(); i++)
	if (new_state[i] != my_state[i]) {
	  int old_val = my_state[i];
	  int new_val = new_state[i];
	  cout << g_variable_name[i] << " changed from " << old_val
	       << " to " << new_val << endl;
	  any_change = true;
	}
      if (!any_change) {
	cout << "new state is the same!" << endl;
      }
      if (verbosity > 1) {
	cout << "new state:" << endl;
	//new_state.dump_fdr();
	new_state.dump_pddl();
      }
      my_state = new_state;
    }
  }

  cout << "reached end of plan" << endl;

  bool goal_ok = test_goal(my_state);
  if (goal_ok) {
    cout << "goal is true!" << endl;
  }
  else {
    cout << "goal not true:" << endl;
    for (int i = 0; i < g_goal.size(); i++) {
      if (my_state[g_goal[i].first] != g_goal[i].second) {
	int g_val = g_goal[i].second;
	int s_val = my_state[g_goal[i].first];
	cout << "variable #" << g_goal[i].first
	     << ", " << g_variable_name[g_goal[i].first]
	     << ", should be " << g_val
	     << " but is " << s_val
	     << endl;
      }
    }
  }

  return 0;
}
