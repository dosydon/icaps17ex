#ifndef SEARCH_ENGINES_TAU_SEARCH_H
#define SEARCH_ENGINES_TAU_SEARCH_H

#include "../search_engine.h"

#include "../open_lists/open_list.h"
#include "../open_lists/open_list_factory.h"

#include <memory>
#include <vector>
#include <set>

class GlobalOperator;
class Heuristic;
class PruningMethod;
class ScalarEvaluator;

namespace options {
class Options;
}

namespace tau_search {
class TauSearch : public SearchEngine {
    const bool reopen_closed_nodes;
    const bool use_multi_path_dependence;

    std::unique_ptr<StateOpenList> open_list;
    ScalarEvaluator *f_evaluator;

    std::vector<Heuristic *> heuristics;
    std::vector<Heuristic *> preferred_operator_heuristics;

    std::shared_ptr<PruningMethod> pruning_method;
	std::shared_ptr<ScalarEvaluator> tau_evaluator;
    std::shared_ptr<OpenListFactory> tau_open_list_factory;


    std::pair<SearchNode, bool> fetch_next_node();
    void start_f_value_statistics(EvaluationContext &eval_context);
    void update_f_value_statistics(const SearchNode &node);
    void reward_progress();
    void print_checkpoint_line(int g) const;
	void apply_observable(const GlobalOperator *op, GlobalState& s,SearchNode& node,std::set<const GlobalOperator*>& preferred_ops,StateID parent_state_id);
    bool  tau_step( const SearchNode& ex_node,const GlobalState& ex_state);

protected:
    virtual void initialize() override;
    virtual SearchStatus step() override;

public:
    explicit TauSearch(const options::Options &opts);
    virtual ~TauSearch() = default;

    virtual void print_statistics() const override;

    void dump_search_space() const;
};
}

#endif
