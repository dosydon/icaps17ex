#ifndef EVALUATORS_TAU_EVALUATOR_H
#define EVALUATORS_TAU_EVALUATOR_H

#include "../scalar_evaluator.h"

class Heuristic;

namespace tau_evaluator {
class TauEvaluator : public ScalarEvaluator {
public:
    TauEvaluator() = default;
    virtual ~TauEvaluator() override = default;

    virtual EvaluationResult compute_result(
        EvaluationContext &eval_context) override;

    virtual void get_involved_heuristics(std::set<Heuristic *> &) override {}
};
}

#endif
