from collections import defaultdict
def compute_groups(task,actions,mutex_groups):
    result = []
    threats = defaultdict(set)
    for action in actions:
        for cond,atom in action.del_effects:
            threats[atom].add(action)
    for invariant in mutex_groups:
        if is_balanced(invariant,threats):
            result.append(invariant)
    return result

def is_balanced(invariant,threats):
        for atom in invariant:
            for threat in threats[atom]:
                if count_add(invariant,threat) < count_del(invariant,threat):
                    return False
        return True

def count_add(invariant,op):
    add_effects = set([atom for cond,atom in op.add_effects])
    return len([atom for atom in invariant if atom in add_effects])

def count_del(invariant,op):
    del_effects = set([atom for cond,atom in op.del_effects])
    return len([atom for atom in invariant if atom in del_effects])
