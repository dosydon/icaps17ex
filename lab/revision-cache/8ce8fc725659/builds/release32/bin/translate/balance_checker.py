from __future__ import print_function

from collections import deque, defaultdict
import itertools
import time

import constraints
import options
import pddl

def get_literals(condition):
    if isinstance(condition, pddl.Literal):
        yield condition
    elif isinstance(condition, pddl.Conjunction):
        for literal in condition.parts:
            yield literal




def find_unique_variables(action, invariant):
    # find unique names for invariant variables
    params = set([p.name for p in action.parameters])
    for eff in action.effects:
        params.update([p.name for p in eff.parameters])
    inv_vars = []
    counter = itertools.count()
    for _ in range(invariant.arity()):
        while True:
            new_name = "?v%i" % next(counter)
            if new_name not in params:
                inv_vars.append(new_name)
                break
    return inv_vars

class BalanceChecker(object):
    def __init__(self):
        self.predicates_to_actions = defaultdict(set)
        self.action_to_heavy_action = {}
    def add_inequality_preconds(self, action, reachable_action_params):
        if reachable_action_params is None or len(action.parameters) < 2:
            return action
        inequal_params = []
        combs = itertools.combinations(range(len(action.parameters)), 2)
        for pos1, pos2 in combs:
            for params in reachable_action_params[action]:
                if params[pos1] == params[pos2]:
                    break
            else:
                inequal_params.append((pos1, pos2))

        if inequal_params:
            precond_parts = [action.precondition]
            for pos1, pos2 in inequal_params:
                param1 = action.parameters[pos1].name
                param2 = action.parameters[pos2].name
                new_cond = pddl.NegatedAtom("=", (param1, param2))
                precond_parts.append(new_cond)
            precond = pddl.Conjunction(precond_parts).simplified()
            return pddl.Action(
                action.name, action.parameters, action.num_external_parameters,
                precond, action.effects, action.cost)
        else:
            return action

    def get_threats(self, predicate):
        return self.predicates_to_actions.get(predicate, set())

    def get_heavy_action(self, action):
        return self.action_to_heavy_action[action]

class MutexBalanceChecker(BalanceChecker):
    def __init__(self, task, reachable_action_params):
        super(MutexBalanceChecker,self).__init__()
        for act in task.actions:
            action = self.add_inequality_preconds(act, reachable_action_params)
            too_heavy_effects = []
            create_heavy_act = False
            heavy_act = action
            for eff in action.effects:
                too_heavy_effects.append(eff)
                if eff.parameters: # universal effect
                    create_heavy_act = True
                    too_heavy_effects.append(eff.copy())
                if not eff.literal.negated:
                    predicate = eff.literal.predicate
                    self.predicates_to_actions[predicate].add(action)
            if create_heavy_act:
                heavy_act = pddl.Action(action.name, action.parameters,
                                        action.num_external_parameters,
                                        action.precondition, too_heavy_effects,
                                        action.cost)
            # heavy_act: duplicated universal effects and assigned unique names
            # to all quantified variables (implicitly in constructor)
            self.action_to_heavy_action[action] = heavy_act

    def check_balance(self, invariant, enqueue_func=None):
        # Check balance for this hypothesis.
        actions_to_check = set()
        for part in invariant.parts:
            actions_to_check |= self.get_threats(part.predicate)
        for action in actions_to_check:
            heavy_action = self.get_heavy_action(action)
            if self.operator_too_heavy(invariant,heavy_action):
                print("heavy")
                print(invariant)
                heavy_action.dump()
                return False
            if self.operator_unbalanced(invariant,action, enqueue_func):
                return False
        return True

    @staticmethod
    def operator_too_heavy(invariant, h_action):
        add_effects = [eff for eff in h_action.effects
                       if not eff.literal.negated and
                          invariant.predicate_to_part.get(eff.literal.predicate)]
        inv_vars = find_unique_variables(h_action, invariant)

        if len(add_effects) <= 1:
            return False

        for eff1, eff2 in itertools.combinations(add_effects, 2):
            system = constraints.ConstraintSystem()
            constraints.ensure_inequality(system, eff1.literal, eff2.literal)
            constraints.ensure_cover(system, eff1.literal, invariant, inv_vars)
            constraints.ensure_cover(system, eff2.literal, invariant, inv_vars)
            constraints.ensure_conjunction_sat(system, get_literals(h_action.precondition),
                                   get_literals(eff1.condition),
                                   get_literals(eff2.condition),
                                   [eff1.literal.negate()],
                                   [eff2.literal.negate()])
            if system.is_solvable():
                return True
        return False

    @staticmethod
    def operator_unbalanced(invariant, action, enqueue_func):
        inv_vars = find_unique_variables(action, invariant)
        relevant_effs = [eff for eff in action.effects
                         if invariant.predicate_to_part.get(eff.literal.predicate)]
        add_effects = [eff for eff in relevant_effs
                       if not eff.literal.negated]
        del_effects = [eff for eff in relevant_effs
                       if eff.literal.negated]
        for eff in add_effects:
            if MutexBalanceChecker.add_effect_unbalanced(invariant,action, eff, del_effects, inv_vars,
                                          enqueue_func):
                return True
        return False

    @staticmethod
    def add_effect_unbalanced(invariant, action, add_effect, del_effects,
                              inv_vars, enqueue_func):

        minimal_renamings = invariant.minimal_covering_renamings(action, add_effect,
                                                            inv_vars)

        lhs_by_pred = defaultdict(list)
        for lit in itertools.chain(get_literals(action.precondition),
                                   get_literals(add_effect.condition),
                                   get_literals(add_effect.literal.negate())):
            lhs_by_pred[lit.predicate].append(lit)

        for del_effect in del_effects:
            minimal_renamings = MutexBalanceChecker.unbalanced_renamings(invariant,del_effect, add_effect,
                inv_vars, lhs_by_pred, minimal_renamings)
            if not minimal_renamings:
                return False

        # Otherwise, the balance check fails => Generate new candidates.
        invariant.refine_candidate(add_effect, action, enqueue_func)
        return True

    @staticmethod
    def unbalanced_renamings(invariant, del_effect, add_effect,
        inv_vars, lhs_by_pred, unbalanced_renamings):
        """returns the renamings from unbalanced renamings for which
           the del_effect does not balance the add_effect."""

        system = constraints.ConstraintSystem()
        constraints.ensure_cover(system, del_effect.literal, invariant, inv_vars)

        # Since we may only rename the quantified variables of the delete effect
        # we need to check that "renamings" of constants are already implied by
        # the unbalanced_renaming (of the of the operator parameters). The
        # following system is used as a helper for this. It builds a conjunction
        # that formulates that the constants are NOT renamed accordingly. We
        # below check that this is impossible with each unbalanced renaming.
        check_constants = False
        constant_test_system = constraints.ConstraintSystem()
        for a,b in system.combinatorial_assignments[0][0].equalities:
            # first 0 because the system was empty before we called ensure_cover
            # second 0 because ensure_cover only adds assignments with one entry
            if b[0] != "?":
                check_constants = True
                neg_clause = constraints.NegativeClause([(a,b)])
                constant_test_system.add_negative_clause(neg_clause)

        constraints.ensure_inequality(system, add_effect.literal, del_effect.literal)

        still_unbalanced = []
        for renaming in unbalanced_renamings:
            if check_constants:
                new_sys = constant_test_system.combine(renaming)
                if new_sys.is_solvable():
                    # it is possible that the operator arguments are not
                    # mapped to constants as required for covering the delete
                    # effect
                    still_unbalanced.append(renaming)
                    continue

            new_sys = system.combine(renaming)
            if invariant.lhs_satisfiable(renaming, lhs_by_pred):
                implies_system = invariant.imply_del_effect(del_effect, lhs_by_pred)
                if not implies_system:
                    still_unbalanced.append(renaming)
                    continue
                new_sys = new_sys.combine(implies_system)
            if not new_sys.is_solvable():
                still_unbalanced.append(renaming)
        return still_unbalanced

class AtLeastBalanceChecker(BalanceChecker):
    def __init__(self, task, reachable_action_params):
        super(AtLeastBalanceChecker,self).__init__()
        for act in task.actions:
            action = self.add_inequality_preconds(act, reachable_action_params)
            too_heavy_effects = []
            create_heavy_act = False
            heavy_act = action
            for eff in action.effects:
                too_heavy_effects.append(eff)
                if eff.parameters: # universal effect
                    create_heavy_act = True
                    too_heavy_effects.append(eff.copy())
                if  eff.literal.negated:
                    predicate = eff.literal.predicate
                    self.predicates_to_actions[predicate].add(action)
            if create_heavy_act:
                heavy_act = pddl.Action(action.name, action.parameters,
                                        action.num_external_parameters,
                                        action.precondition, too_heavy_effects,
                                        action.cost)
            # heavy_act: duplicated universal effects and assigned unique names
            # to all quantified variables (implicitly in constructor)
            self.action_to_heavy_action[action] = heavy_act
    def check_balance(self, invariant, enqueue_func=None):
        # Check balance for this hypothesis.
        actions_to_check = set()
        for part in invariant.parts:
            actions_to_check |= self.get_threats(part.predicate)
        for action in actions_to_check:
            heavy_action = self.get_heavy_action(action)
            if self.operator_too_heavy(invariant,heavy_action):
                print("heavy")
                print(invariant)
                heavy_action.dump()
                return False
            if self.operator_unbalanced(invariant,action, enqueue_func):
                return False
        return True

    @staticmethod
    def operator_too_heavy(invariant, h_action):
        del_effects = [eff for eff in h_action.effects
                       if eff.literal.negated and
                          invariant.predicate_to_part.get(eff.literal.predicate)]
        inv_vars = find_unique_variables(h_action, invariant)

        if len(del_effects) <= 1:
            return False

        for eff1, eff2 in itertools.combinations(del_effects, 2):
            system = constraints.ConstraintSystem()
            constraints.ensure_inequality(system, eff1.literal, eff2.literal)
            constraints.ensure_cover(system, eff1.literal, invariant, inv_vars)
            constraints.ensure_cover(system, eff2.literal, invariant, inv_vars)
            constraints.ensure_conjunction_sat(system, get_literals(h_action.precondition),
                                   get_literals(eff1.condition),
                                   get_literals(eff2.condition),
                                   [eff1.literal.negate()],
                                   [eff2.literal.negate()])
            if system.is_solvable():
                return True
        return False

    @staticmethod
    def operator_unbalanced(invariant, action, enqueue_func):
        return False


