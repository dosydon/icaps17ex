
from gurobipy import *
from collections import defaultdict
def choose_groups_exact(groups, reachable_facts):
    coverable_facts = set()
    for group in groups:
        coverable_facts.update(group)
    uncovered_facts = reachable_facts - coverable_facts
    print(len(uncovered_facts), "uncovered facts")
    result = []

    try:
        choice_var = {}
        hit_set = defaultdict(set)

        # Create a new model
        m = Model("mip1")
        m.setParam(GRB.Param.Threads,1)

        for i,group in enumerate(groups):
            group = tuple(group)
            choice_var[group] = m.addVar(vtype=GRB.BINARY,name=str("group" + str(i)))
            for fact in group:
                hit_set[fact].add(group)

        # Integrate new variables
        m.update()

        obj = sum([var for var in choice_var.values()])
        # Set objective
        m.setObjective(obj, GRB.MINIMIZE)

        for fact in coverable_facts:
            cst = sum([choice_var[group] for group in hit_set[fact]])
            m.addConstr(cst >= 1)

        # Add constraint: x + y >= 1
#         m.addConstr(x + y >= 1, "c1")

        m.optimize()

        covered_facts = set()
        for group,v in choice_var.items():
            if v.x >=1:
                tmp = []
                for item in group:
                    if not item in covered_facts:
                        tmp.append(item)
                covered_facts.update(group)
                result.append(tmp)

        print('Obj: %g' % m.objVal)

        result += [[fact] for fact in uncovered_facts]
        return result

    except GurobiError as e:
        print('Error reported')
        print(e.message)

def choose_groups_essential_exact(groups, exactly1,reachable_facts):
    groups = ([tuple(group) for group in groups])
    exactly1 = ([tuple(group) for group in exactly1])

    coverable_facts = set()
    for group in groups:
        coverable_facts.update(group)
    uncovered_facts = reachable_facts - coverable_facts
    print(len(uncovered_facts), "uncovered facts")
    result = []

    try:
        choice_group = {}
        choice_delete = defaultdict(dict)
        hit_set = defaultdict(set)
        hit_set_delete = defaultdict(set)

        # Create a new model
        m = Model("mip1")
        m.setParam(GRB.Param.Threads,1)

        for i,group in enumerate(groups):
            choice_group[group] = m.addVar(vtype=GRB.BINARY,name=str("group" + str(i)))
            for fact in group:
                hit_set[fact].add(group)

        for i,group in enumerate(exactly1):
            for fact in group:
                choice_delete[group][fact] = m.addVar(vtype=GRB.BINARY,name=str("exactly_group" + str(i) + str(fact)))
                hit_set_delete[fact].add((group,fact))

        m.update()

        obj = sum([var for var in choice_group.values()])
        # Set objective
        m.setObjective(obj, GRB.MINIMIZE)

        for fact in coverable_facts:
            cst = sum(choice_group[fact] for fact in hit_set[fact])
            cst += sum(choice_delete[group][fact] for group,fact in hit_set_delete[fact])
            m.addConstr(cst >= 1)
        for group in exactly1:
            cst = sum([choice_delete[group][fact] for fact in group])
            m.addConstr(cst <= 1)

        m.optimize()
#
#         covered_facts = set()
#         for group,v in choice_group.items():
#             if v.x >=1:
#                 print(group)
#         for group,inner in choice_delete.items():
#             for fact,v in inner.items():
#                 if v.x >=1:
#                     print("inessential")
#                     print(fact)
#                 tmp = []
#                 for item in group:
#                     if not item in covered_facts:
#                         tmp.append(item)
#                 covered_facts.update(group)
#                 result.append(tmp)

        print('Obj: %g' % m.objVal)
        return int(m.objVal)
#
#         result += [[fact] for fact in uncovered_facts]
#         return result

    except GurobiError as e:
        print('Error reported')
        print(e.message)
