#include "tau_pref_heuristic.h"

#include "../global_state.h"
#include "../globals.h"
#include "../global_operator.h"
#include "../option_parser.h"
#include "../plugin.h"
#include "../task_tools.h"

#include <cstddef>
#include <limits>
#include <utility>

using namespace std;

namespace tau_pref_heuristic {
TauPrefHeuristic::TauPrefHeuristic(const Options &opts)
    : Heuristic(opts) {
    min_operator_cost = numeric_limits<int>::max();
    for (OperatorProxy op : task_proxy.get_operators())
        min_operator_cost = min(min_operator_cost, op.get_cost());
}

TauPrefHeuristic::~TauPrefHeuristic() {
}

void TauPrefHeuristic::initialize() {
    cout << "Initializing blind search heuristic..." << endl;
}

int TauPrefHeuristic::compute_heuristic(const GlobalState &global_state) {
		global_state.get_id();
		for (OperatorProxy op : task_proxy.get_operators()){
			if(!op.get_global_operator()->is_tau())
				set_preferred(op);
		}
        return 0;
}

static Heuristic *_parse(OptionParser &parser) {
    parser.document_synopsis("Blind heuristic",
                             "Returns cost of cheapest action for "
                             "non-goal states, "
                             "0 for goal states");
    parser.document_language_support("action costs", "supported");
    parser.document_language_support("conditional effects", "supported");
    parser.document_language_support("axioms", "supported");
    parser.document_property("admissible", "yes");
    parser.document_property("consistent", "yes");
    parser.document_property("safe", "yes");
    parser.document_property("preferred operators", "no");

    Heuristic::add_options_to_parser(parser);
    Options opts = parser.parse();
    if (parser.dry_run())
        return 0;
    else
        return new TauPrefHeuristic(opts);
}

static Plugin<Heuristic> _plugin("tau_pref", _parse);
}
