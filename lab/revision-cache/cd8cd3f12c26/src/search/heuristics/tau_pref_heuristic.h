#ifndef HEURISTICS_TAU_PREF__HEURISTIC_H
#define HEURISTICS_TAU_PREF__HEURISTIC_H

#include "../heuristic.h"

namespace tau_pref_heuristic {
class TauPrefHeuristic : public Heuristic {
    int min_operator_cost;
protected:
    virtual void initialize();
    virtual int compute_heuristic(const GlobalState &global_state);
public:
    TauPrefHeuristic(const options::Options &options);
    ~TauPrefHeuristic();
};
}

#endif
