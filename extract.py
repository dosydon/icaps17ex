#! /usr/bin/env python

"""Solve some tasks with A* and the LM-Cut heuristic."""

import os.path
import platform
import downward
from lab.fetcher import Fetcher
from lab.steps import Step
from lab.environments import LocalEnvironment, MaiaEnvironment
from downward.reports.absolute import AbsoluteReport
from downward import suites
from lab.steps import Step
from lab.reports import Report
from lab.experiment import Experiment
from downward.reports.taskwise import TaskwiseReport
from lab.reports.filter import FilterReport

EXPPATH = 'data/extract/'
BENCH = os.path.abspath('benchmarks')
TENCODE=os.path.abspath('translate_and_encode.py')
DIR = os.path.dirname(os.path.abspath(__file__))
ATTRIBUTES = ['translator_essentials','translator_inessentials','coverage','invalid_input','encode_returncode','encode_peak_memory','encode_wall_clock_time',"removed_operators",'candidate_time']
SUITE = ['miconic','pegsol-opt11-strips','satellite','tpp','barman-opt14-strips','blocks','depot','driverlog','freecell','mystery','parcprinter-opt11-strips','parking-opt14-strips','rovers','storage','tidybot-opt14-strips','woodworking-opt11-strips','airport','grid','gripper','scanalyzer-opt11-strips','sokoban-opt08-strips','visitall-opt14-strips']
#SUITE = ['pegsol-08-strips','pegsol-opt11-strips','barman-opt11-strips','barman-opt14-strips','parking-opt11-strips','parking-opt14-strips','tidybot-opt11-strips','tidybot-opt14-strips','woodworking-opt08-strips','woodworking-opt11-strips','sokoban-opt08-strips','sokoban-opt11-strips','visitall-opt11-strips','visitall-opt14-strips']
# SUITE += downward.suites.suite_ipc98_to_ipc04()
# SUITE += downward.suites.suite_ipc06()
# SUITE += downward.suites.suite_ipc08_opt()
# SUITE += downward.suites.suite_ipc08_sat()
# SUITE += downward.suites.suite_ipc11_sat()
# SUITE += downward.suites.suite_ipc11_opt()
# SUITE += downward.suites.suite_ipc14_opt()
TIME_LIMIT=300
MEM_LIMIT= 2048

ENV=LocalEnvironment(processes=24)
exp = Experiment(EXPPATH,environment=ENV)

for suite in SUITE:
    for prob in suites.build_suite(BENCH,[suite]):
        config = 'var based'
        run = exp.add_run()
        run.add_resource('PROBLEM', prob.problem_file())
        run.add_resource('DOMAIN', prob.domain_file())
        run.add_command('encode', [TENCODE,'DOMAIN','PROBLEM','--candidate_gen','top'])
        run.set_property('id', [suite,prob.problem_file(),config])
        run.set_property('domain', suite)
        run.set_property('config', config)
        run.set_property('problem', prob.problem_file())

        config = 'graph based'
        run = exp.add_run()
        run.add_resource('PROBLEM', prob.problem_file())
        run.add_resource('DOMAIN', prob.domain_file())
        run.add_command('encode', [TENCODE,'DOMAIN','PROBLEM','--candidate_gen','opgraph'])
        run.set_property('id', [suite,prob.problem_file(),config])
        run.set_property('domain', suite)
        run.set_property('config', config)
        run.set_property('problem', prob.problem_file())

        config = 'var based essential'
        run = exp.add_run()
        run.add_resource('PROBLEM', prob.problem_file())
        run.add_resource('DOMAIN', prob.domain_file())
        run.add_command('encode', [TENCODE,'DOMAIN','PROBLEM','--candidate_gen','top','translate','--group_choice essential --axiom'])
        run.set_property('id', [suite,prob.problem_file(),config])
        run.set_property('domain', suite)
        run.set_property('config', config)
        run.set_property('problem', prob.problem_file())

        config = 'essential'
        run = exp.add_run()
        run.add_resource('PROBLEM', prob.problem_file())
        run.add_resource('DOMAIN', prob.domain_file())
        run.add_command('encode', [TENCODE,'DOMAIN','PROBLEM','translate','--group_choice essential --axiom'])
        run.set_property('id', [suite,prob.problem_file(),config])
        run.set_property('domain', suite)
        run.set_property('config', config)
        run.set_property('problem', prob.problem_file())

        config = 'default'
        run = exp.add_run()
        run.add_resource('PROBLEM', prob.problem_file())
        run.add_resource('DOMAIN', prob.domain_file())
        run.add_command('encode', [TENCODE,'DOMAIN','PROBLEM'])
        run.set_property('id', [suite,prob.problem_file(),config])
        run.set_property('domain', suite)
        run.set_property('config', config)
        run.set_property('problem', prob.problem_file())



exp.add_step(Step('fetcher', Fetcher(), exp.path, exp.eval_dir,
                  parsers=os.path.join(DIR, 'extract_parser.py')))

# exp.add_step(Step('filter',
#                   FilterReport(filter=valid),
#                   exp.eval_dir, exp.eval_dir + '/properties'))
# Make a report (AbsoluteReport is the standard report).
exp.add_report(
    AbsoluteReport(attributes=ATTRIBUTES), outfile='report.html')


exp()
