#! /usr/bin/env python

from lab.parser import Parser
import re

def coverage(content,prop):
    if 'Solution Found!' in content:
        prop['coverage'] = 1
    else:
        prop['coverage'] = 0

def solution_depth(content,prop):
    prop["depth"] = None
    m = re.search('solution_depth:(\d+)',content)
    if m:
        prop["depth"] = int(m.group(1))

def maxrss(content,prop):
    prop["maxrss"] = None
    m = re.search('MAXRSS:(\d+)',content)
    if m:
        prop["maxrss"] = int(m.group(1))

def log(content,prop):
    attrs = ["CLINGO","HP","CLASP","GRINGO","ENCODE","MZN","PREPROCESS","TRANSLATE","DECODE","VAL"]
    for attr in attrs:
        m = re.search(attr +'\s*:(\d+.\d+)',content)
        if m:
            prop[attr] = float(m.group(1))

def stats(content,prop):
    matches = re.findall('"Solve": (\d+.\d+)',content)
    if matches:
        prop["solve"] = 0
        for m in matches:
            prop["solve"] += float(m)
    else:
        prop["solve"] = None

def validate(content,prop):
    if 'Successful plans' in content:
        prop['validate'] = 1
    elif 'Failed plans' in content and prop['coverage'] == 1:
        prop['validate'] = -1
    else:
        prop['validate'] = 0

parser = Parser()
parser.add_function(coverage)
parser.add_function(solution_depth)
parser.add_function(maxrss)
parser.add_function(log)
parser.add_function(stats)
parser.add_function(validate)
parser.add_pattern('makespan','solution_depth:(\d+)',required=False)
parser.parse()
