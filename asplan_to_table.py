#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import json
from pprint import pprint
from collections import defaultdict
from json_to_table import *

configs = ['ASPLAN','ASPLANS','ASPlAN+E','ASPlAN+ET','ASPlAN+ET+']
domains = ['miconic','pegsol-opt11-strips','satellite','scanalyzer-opt11-strips','tpp','barman-opt14-strips','blocks','depot','driverlog','freecell','mystery','parcprinter-opt11-strips','parking-opt14-strips','rovers','storage','tidybot-opt14-strips','woodworking-opt11-strips','airport','grid','gripper','sokoban-opt08-strips','visitall-opt14-strips']
columns = [('ASPLAN','coverage'),('ASPLAN','n'),('ASPLANS','coverage'),('ASPLANS','n'),('ASPlAN+E','coverage'),('ASPlAN+E','n'),('ASPlAN+ET','coverage'),('ASPlAN+ET','n'),('ASPlAN+ET','decode'),('ASPlAN+ET+','coverage'),('ASPlAN+ET+','n'),('ASPlAN+ET+','decode')]

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("properties",help="json file to read")
    args = parser.parse_args()

    table = {}

    with open(args.properties) as f:    
        data = json.load(f)
        is_all_solved = get_all_solved(data)

        for config in configs:
            for domain in domains:
                inst_lst = (get_with_keyvals(data, config = config, domain = domain))
                table[(domain, (config, 'coverage'))] = str(get_coverage(inst_lst))
                table[(domain, (config, 'n'))] = (get_average_n(inst_lst, is_all_solved))
                table[(domain, (config, 'decode'))] = (get_average_decode(inst_lst))
                table[(domain,'name')] = '{}({})'.format(domain,len(inst_lst))

            inst_lst = (get_with_keyvals(data, config = config))
            table[('sum', (config, 'coverage'))] = str(get_coverage(inst_lst))
            table[('sum','name')] = '{}({})'.format('sum',len(inst_lst))
            table[('sum', (config, 'n'))] = (get_average_n(inst_lst, is_all_solved))
            table[('sum', (config, 'decode'))] = (get_average_decode(inst_lst))

        sum = 0
        for domain in domains:
            coverages = [int(table[(domain, (config, 'coverage'))]) for config in configs]
            table[(domain, ('ideal', 'coverage'))] = str(max(coverages))
            sum += max(coverages)
            for config in configs:
                if int(table[(domain, (config,'coverage'))]) == max(coverages):
                    table[(domain, (config, 'coverage'))] = "{\\bf " + str(max(coverages)) + " }"
        table[('sum', ('ideal', 'coverage'))] = str(sum)



    to_tex_table(table, domains + ['sum'], columns + [('ideal','coverage')])
