#About
This repository contains the codes and scripts needed for the experiments in the paper "Automatic Extraction of Axioms in Planning".

##Table1
- extract.py
- extract2.py
- extract_fetcher

##Table2
- iplan_t_variants.py
- iplan_e.py
- iplan.py
- scv.py
- iplan_t_plus.py
- iplan_miconic.py
- iplan_s.py
- iplan_fetcher (fetch all the results)

##Table3
- asplan.py
- asplan_et.py
- asplan_et_plus.py
- asplan_rovers.py
- asplan_fetcher.py ( fetch all the resutls)
