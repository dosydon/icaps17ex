#! /usr/bin/env python

""" Example lab experiment that approximates the number pi.  This file contains the advanced version of the experiment where pi is calculated with increasing precision.  This experiment builds on the basic pi.py experiment.  """ 
import os
from lab.fetcher import Fetcher

from downward.reports.absolute import AbsoluteReport
from lab.experiment import Experiment
from lab.steps import Step
from lab.reports import Report
from lab.reports.filter import FilterReport
from lab.environments import LocalEnvironment
from downward.suites import suite_all,build_suite
from downward.reports.scatter import ScatterPlotReport


EXPPATH = 'data/iplan_fetcher/'
TIME_LIMIT = 300
MEM_LIMIT = 2048

ATTRIBUTES = ['decode','validate','coverage','makespan','hp_wall_clock_time','maxrss']
BENCHMARKS = os.path.expanduser('~/benchmarks')
ENV=LocalEnvironment(processes=24)
DIR = os.path.dirname(os.path.abspath(__file__))

exp = Experiment(EXPPATH,environment=ENV)

#exp.add_step(Step('iplan-fetcher', Fetcher(), 'data/iplan', exp.eval_dir, parsers=os.path.join(DIR, 'hp_parser.py')))
#exp.add_step(Step('iplan-fetcher', Fetcher(), 'data/iplan_t', exp.eval_dir, parsers=os.path.join(DIR, 'hp_parser.py')))
exp.add_step(Step('iplan-fetcher', Fetcher(), 'data/iplan_t_variants', exp.eval_dir, parsers=os.path.join(DIR, 'hp_parser.py')))
exp.add_step(Step('iplan-fetcher', Fetcher(), 'data/iplan_e', exp.eval_dir, parsers=os.path.join(DIR, 'hp_parser.py')))
exp.add_step(Step('iplan-fetcher', Fetcher(), 'data/iplan', exp.eval_dir, parsers=os.path.join(DIR, 'hp_parser.py')))
exp.add_step(Step('iplan-fetcher', Fetcher(), 'data/scv', exp.eval_dir, parsers=os.path.join(DIR, 'hp_parser.py')))
exp.add_step(Step('iplan-fetcher', Fetcher(), 'data/iplan_t_plus', exp.eval_dir, parsers=os.path.join(DIR, 'hp_parser.py')))
exp.add_step(Step('iplan-fetcher', Fetcher(), 'data/iplan_miconic', exp.eval_dir, parsers=os.path.join(DIR, 'hp_parser.py')))
exp.add_step(Step('iplan-fetcher', Fetcher(), 'data/iplan_s', exp.eval_dir, parsers=os.path.join(DIR, 'hp_parser.py')))

exp.add_report(AbsoluteReport(attributes=ATTRIBUTES), outfile='report.html')

def get_domain(run1, run2):
    domain = run1['domain']
    if domain.endswith('-strips'):
        return domain[:-7]
    else:
        return domain

params={
#         'font.size':20,
        'legend.fontsize':18,
        'axes.labelsize':24,
        'axes.titlesize':24,
        'xtick.labelsize':20,
        'ytick.labelsize':20,
        'lines.markiersize':15,
        'legend.markersize':2.0,
        }

exp.add_report(
    ScatterPlotReport(
        params=params,
        xscale='linear',
        yscale='linear',
        xlabel='IPlan',
        ylabel= 'IPlan(t,e)+',
        attributes=['makespan'],
        get_category=get_domain,
        filter_config=['IPLAN', 'IPLAN+ET+']),
    outfile=os.path.join('plots', 'simple-smart.png'))

exp.add_report(
    ScatterPlotReport(
        params=params,
        xscale='linear',
        yscale='linear',
        xlabel='IPlan',
        ylabel= 'IPlan(t,e)',
        attributes=['makespan'],
        get_category=get_domain,
        filter_config=['IPLAN', 'IPLAN+ET']),
    outfile=os.path.join('plots', 'simple-et.png'))

exp()
